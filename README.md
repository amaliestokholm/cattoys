# Cattoys

# IMPORTANT: This directory has now been merged with Skycats and is no longer maintained from this channel.

These scripts include tools for working efficiently with compiled catalogues from Skycats.

The scripts do depend on some of the scripts in Skycats, so access to Skycats is needed to use the tools in this repository.
For access to the Skycats repository, contact Amalie (stokholm at phys au dk).

## General notes on using Cattoys

This git repository contains some tools for data analysing, computing dynamic quantities, use clustering algorithms, and plot data from the Skycats project. The `cattoys` directory contains different scripts for different objectives:
- `cutcat` contains functions used for selection criterias and removing bad entries in the catalogues.
- `drawcat` contains functions used for plotting.
- `dynacat` contains functions for computing kinematic properties of the stars.
- `robocat` contains functions used in clustering/trend searching in the catalogues.
- `toolcat` contains small functions that does not fit in the other files.

In order to run the scripts, one needs to add the skycats directory to the $(PYTHONPATH) by adding the following to the bashrc file.
```
export PYTHONPATH=${PYTHONPATH}:${HOME}/cattoys
```


[Anders+2015]: https://ui.adsabs.harvard.edu/abs/2017A%26A...597A..30A/abstract
[Casagrande+2014]: https://ui.adsabs.harvard.edu/abs/2014ApJ...787..110C/abstract
[Cutri+2003]: https://irsa.ipac.caltech.edu/Missions/2mass.html
[Gaia Collaboration+2016]: https://ui.adsabs.harvard.edu/abs/2016A%26A...595A...1G/abstract
[Gaia Collaboration+2018]: https://ui.adsabs.harvard.edu/abs/2018A%26A...616A...1G/abstract
[Deleuil+2009]: https://ui.adsabs.harvard.edu/abs/2009AJ....138..649D/abstract
[Huber+2017]: http://adsabs.harvard.edu/abs/2017yCat.4034....0H
[Høg+2000]: https://ui.adsabs.harvard.edu/abs/2000A%26A...355L..27H/abstract
[MAST]: http://archive.stsci.edu/kepler/data\_search/search.php
[Mathur+2017]: https://ui.adsabs.harvard.edu/abs/2017ApJS..229...30M/abstract
[Meunier+2007]: https://ui.adsabs.harvard.edu/abs/2009AJ....138..649D/abstract
[Price-Whelan+2018]: https://ui.adsabs.harvard.edu/abs/2018AJ....156...18P/abstract
[Salaris+2002]: https://ui.adsabs.harvard.edu/abs/2002PASP..114..375S/abstract
[Salaris+2018]: https://ui.adsabs.harvard.edu/abs/2018A%26A...612A..68S/abstract
[Sanders+2018]: https://ui.adsabs.harvard.edu/abs/2018MNRAS.481.4093S/abstract
[Skrutskie+2006]: http://adsabs.harvard.edu/abs/2006AJ....131.1163S
[Silva Aguirre+2015]: https://ui.adsabs.harvard.edu/abs/2015MNRAS.452.2127S/abstract
[Silva Aguirre+2017]: https://ui.adsabs.harvard.edu/abs/2017ApJ...835..173S/abstract
[Stello+2017]: https://arxiv.org/abs/1611.09852
[Vrard+2016]: https://ui.adsabs.harvard.edu/abs/2016A%26A...588A..87V/abstract
[Yi+2001]: https://ui.adsabs.harvard.edu/abs/2001ApJS..136..417Y/abstract
[Yu+2018]: https://arxiv.org/abs/1802.04455
[Zacharias+2017]: https://ui.adsabs.harvard.edu/abs/2000A%26A...355L..27H/abstract
