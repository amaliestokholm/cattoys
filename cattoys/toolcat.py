# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# amalie
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###############################################################################
# MODULES
###############################################################################
import numpy as np
from natsort import natsorted
import scipy.interpolate
from mpl_toolkits.axes_grid1 import make_axes_locatable
from astropy.table import Table, Column

from skycats import funkycat


###############################################################################
# FUNCTIONETTES
###############################################################################
def remove_dumbchars(name):
    # Remove annoying characters from generated file names
    name = name.replace('/', '_').replace(' ', '').replace('\\', '')
    name = name.replace('$', '').replace('[', '').replace(']', '')
    name = name.replace('(', '').replace(')', '')
    name = name.replace('{', '').replace('}', '')
    name = name.replace('-', '').replace('+', 'p')
    return name


def colorbar(mappable):
    # from https://joseph-long.com/writing/colorbars/
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    return fig.colorbar(mappable, cax=cax)


def strided_app(a, L, S):
    # https://stackoverflow.com/questions/40084931/taking-subarrays-from-numpy
    # -array-with-given-stride-stepsize/40085052#40085052
    # Window len = L, Stride len/stepsize = S
    nrows = ((a.size - L) // S) + 1
    n = a.strides[0]
    return np.lib.stride_tricks.as_strided(
        a, shape=(nrows, L), strides=(S * n, n))


def get_labels(kitten, fillvalue=''):
    # Get a sorted list of labels in kitten
    # Sort the labels after length
    if isinstance(kitten, Column):
        fillvalue = funkycat.get_fillvalue(kitten)
        labels = np.unique(kitten[kitten != fillvalue])
    else:
        labels = np.unique(kitten['LABEL'][kitten['LABEL'] != str(fillvalue)])
    labels = natsorted(labels)
    #labels_ = list(map(lambda x: len(x), labels))
    #labels = labels[np.argsort(labels_)]
    return labels


def convert_angles(angles):
    angles /= (1000. * 3600.)
    return angles


def in_gyr(ages):
    # In BASTA, ages are given in Myr but sometimes we want to convert to Gyr
    fillvalue = funkycat.get_fillvalue(ages)
    mask = (ages != fillvalue)
    gyrages = ages.copy()
    gyrages[mask] /= 1000
    return gyrages


def compute_quantiles(xs, qs=[0.5, 0.158655, 0.841345]):
    median, m, p = np.quantile(xs, qs)
    return median, median - m, p - median


def calc_ruwe(kitten, fillvalue=-9999):
    print('Compute RUWE')

    def compute_tdtable(u0table):
        # Compute u0 using With colors and gmag
        for i, col in enumerate(u0table.columns[2:]):
            if i == 0:
                tdtable = u0table[col].data.reshape(len(u0table[col].data), 1)
            else:
                coldata = u0table[col].data.reshape(len(u0table[col].data), 1)
                tdtable = np.concatenate((tdtable, coldata), axis=1)

        tdtable = tdtable.T
        return tdtable

    def compute_ruwe(gmag, bprp, chi2, ngoodobs, tdtable, u0table):
        if (bprp > -1.0) & (bprp < 10.0):
            # Read table from DR2_RUWE_V1.zip
            c = np.arange(-1.0, 10+0.1, 0.1)

            m = c.size
            n = u0table['g_mag'].data.size
            assert tdtable.shape == (m, n), (m, n)

            f = scipy.interpolate.interp2d(u0table['g_mag'].data, c, tdtable,
                                           kind='linear')
            u0 = f(gmag, bprp)[0]
            # print('using color and gmag u0=', u0)
        else:
            # print('bp-rp is outside the grid!')
            if (np.isfinite(gmag)) & (gmag > 3.6) & (gmag < 21.0):
                # print('u0 is computed using only gmag')

                # Interpolate in table from DR2_RUWE_V1.zip
                f = scipy.interpolate.interp1d(u0table['g_mag'].data,
                                               u0table['u0g'].data)
                u0 = f(gmag)
                # print('using only gmag u0=', f(gmag))
            else:
                # print('gmag is outside the grid!')
                return fillvalue

        # Compute RUWE
        u = np.sqrt(chi2 / (ngoodobs - 5))
        ruwe = u / u0
        return ruwe

    gmags = kitten['PHOT_G_MEAN_MAG_GAIA'].data
    bprps = kitten['BP_RP_GAIA'].data
    chi2s = kitten['ASTROMETRIC_CHI2_AL_GAIA'].data
    ngoodobss = kitten['ASTROMETRIC_N_GOOD_OBS_AL_GAIA'].data

    ruwe = np.zeros(len(gmags))
    u0table = Table.read(
        '/usr/users/als/cattoys/cattoys/gaiatables/table_u0_2D.txt',
        format='ascii')
    tdtable = compute_tdtable(u0table)
    for i, (gmag, bprp, chi2, ngoodobs) in enumerate(zip(gmags, bprps,
                                                         chi2s, ngoodobss)):
        try:
            if (((bprp > (-1.0)) & (bprp < 10.0)) &
                ((chi2 != fillvalue) & (ngoodobs != fillvalue))):
                ruwe[i] = compute_ruwe(gmag, bprp, chi2, ngoodobs,
                                       tdtable, u0table)
            else:
                ruwe[i] = fillvalue
        except TypeError:
            print(chi2, ngoodobs, fillvalue, bprp, gmag)
            raise
    ruwecol = Column(ruwe, name='RUWE_GAIA')
    kitten.add_column(ruwecol)
    return kitten


def compute_absolutemags(kitten, distances, magnitudes):
    """
    This function computes absolute magnitudes.

    Parameters
    ----------
    kitten : astropy table
        Catalogue which the magnitude column should be added to.
    distances : str
        Name of distance column to use in transformation.
    magnitudes : str
        Name of magnitude column to use in transformation.

    Returns
    -------
    kitten : astropy table
        Catalogue containing a new column with absolute magnitudes.
    """
    dists = kitten[distances].data
    mags = kitten[magnitudes].data

    if distances == 'PARALLAX_GAIA':
        dists *= 1e-3
        dists = (1 / dists)

    absmag = mags + 5 - (5 * np.log10(dists))
    absmagcol = Column(absmag, name='ABS_' + magnitudes)
    kitten.add_column(absmagcol)


def table_to_df(cols, idstrs, units=None, labelcol=None):
    """
    Change a list of astropy columns to a Pandas data frame.
    """
    fillmask = np.ones(len(cols[0].data), dtype=bool)
    for i, c in enumerate(cols):
        fillvalue = funkycat.get_fillvalue(c)
        fillmask &= (c.data != fillvalue)

    if units is None:
        labels = ['%s' % i for i in idstrs]
    else:
        labels = []
        for i, unit in enumerate(units):
            if unit is not None:
                labels.append('%s (%s)' % (idstrs[i], unit))
            else:
                labels.append(idstrs[i])

    # Add label to data so colorcoding is possible
    if labelcol is not None:
        cols.append(labelcol)
        labels.append('Survey')

    adf = Table(cols)[:][fillmask]
    df = adf.to_pandas()
    df.columns = labels
    return df
