# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# amalie
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###############################################################################
# MODULES
###############################################################################
import numpy as np

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.cluster import KMeans
from sklearn.cluster import SpectralClustering
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import AffinityPropagation
from sklearn.cluster import DBSCAN
from sklearn.mixture import GaussianMixture as GMM

# Make some plot settings
sns.set_palette('colorblind')
sns.set_color_codes('colorblind')
sns.set_style({"xtick.direction": "inout", "ytick.direction": "inout",
               "xtick.top": True, "ytick.right": True})
ckepler = '#3498db'
ccorogee = '#800000'


###############################################################################
# Algorithms
###############################################################################
def find_clusters_kmeans(plotsname, X, n=2):
    # Try a simple K means approach
    assert isinstance(plotsname, str)
    name = plotsname + '_kmeans_n' + str(n) + '.pdf'

    kmeans = KMeans(n_clusters=n)
    kmeans.fit(X)
    labels = kmeans.predict(X)

    pp = PdfPages(name)
    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], '.', c=labels, cmap='viridis')
    centers = kmeans.cluster_centers_
    plt.scatter(centers[:, 0], centers[:, 1], c='k', s=200, alpha=0.5)
    pp.savefig(bbox_inches='tight')
    pp.close()


def find_clusters_spectralclustering(plotsname, X, n=2):
    # Try a simple K means approach
    assert isinstance(plotsname, str)
    name = plotsname + '_spectral_n' + str(n) + '.pdf'

    model = SpectralClustering(n, affinity='nearest_neighbors',
                               assign_labels='kmeans')
    labels = model.fit_predict(X)

    pp = PdfPages(name)
    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], '.', c=labels, cmap='viridis')
    pp.savefig(bbox_inches='tight')
    pp.close()


def find_clusters_gmm(plotsname, X, n=2, full=True):
    # Try a simple Gaussian Mixture models
    assert isinstance(plotsname, str)

    if full:
        name = plotsname + '_gmmfull_n' + str(n) + '.pdf'
        model = GMM(n_components=n, covariance_type='full',
                    random_state=42).fit(X)
    else:
        name = plotsname + '_gmm_n' + str(n) + '.pdf'
        model = GMM(n_components=n).fit(X)

    labels = model.predict(X)

    pp = PdfPages(name)
    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], s=1, c=labels, cmap='viridis')
    pp.savefig(bbox_inches='tight')
    pp.close()


def find_n_gmm(plotsname, X, ns=np.arange(1, 21)):
    # Find the number of components that minimizes GMM as a density estimate.
    assert isinstance(plotsname, str)
    name = plotsname + '_gmmfull_findn.pdf'
    models = [GMM(n, covariance_type='full', random_state=0).fit(X)
              for n in ns]

    pp = PdfPages(name)
    plt.figure()
    # Bayesian Information Criterion
    plt.plot(ns, [m.bic(X) for m in models], label='BIC')
    # Akaike information criterion
    plt.plot(ns, [m.aic(X) for m in models], label='AIC')
    plt.legend(loc='best', frameon=False)
    plt.xlabel('n components')
    pp.savefig(bbox_inches='tight')
    pp.close()


def find_clusters_agglomerative(plotsname, X, n=2):
    # Use Agglomerative clusting. It does not need a specified n.
    # https://www.learndatasci.com/tutorials/k-means-clustering-algorithms-python-intro/
    assert isinstance(plotsname, str)

    name = plotsname + '_agglomerative_n' + str(n) + '.pdf'
    model = AgglomerativeClustering(n_clusters=n, linkage="ward").fit(X)

    labels = model.labels_

    pp = PdfPages(name)
    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], s=1, c=labels, cmap='viridis')
    pp.savefig(bbox_inches='tight')
    pp.close()


def find_clusters_affinity(plotsname, X):
    # Use affinity clusting. It needs no specified n.
    # https://www.learndatasci.com/tutorials/k-means-clustering-algorithms-python-intro/
    assert isinstance(plotsname, str)

    name = plotsname + '_affinity.pdf'
    model = AffinityPropagation(damping=0.6).fit(X)

    labels = model.labels_

    pp = PdfPages(name)
    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], s=1, c=labels, cmap='viridis')
    pp.savefig(bbox_inches='tight')
    pp.close()


def find_clusters_dbscan(plotsname, X, eps=0.3, min_samples=10):
    assert isinstance(plotsname, str)

    name = plotsname + '_dbscan.pdf'
    db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)
    labels = db.labels_

    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    print('Estimated number of clusters: %d' % n_clusters_)
    print('Estimated number of noise points: %d' % n_noise_)

    pp = PdfPages(name)
    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], s=1, c=labels, cmap='viridis')
    pp.savefig(bbox_inches='tight')
    pp.close()

