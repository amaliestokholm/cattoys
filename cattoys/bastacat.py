# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# amalie
# Scipts to handle BASTA
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###############################################################################
# MODULES
###############################################################################
import os
import sys
import csv
import numpy as np

from astropy.table import Table, Column, join
import astropy.coordinates as coord

from skycats import funkycat
from cattoys import toolcat as tool
from cattoys import drawcat as draw

# Don't print Astropy warnings
import warnings
from astropy.utils.exceptions import AstropyWarning
warnings.filterwarnings('ignore', category=AstropyWarning)


###############################################################################
# Functions for handling BASTA output
###############################################################################
def make_bastatable(bastafile, restartfile=None):
    assert os.path.isfile(bastafile), bastafile
    bastatable = Table.read(bastafile, format='ascii')
    if 'starid' in bastatable.columns:
        bastatable.rename_column('starid', 'TMASS_ID')
        bastatable = funkycat.standardize(bastatable,
                                          catid='BASTA',
                                          old=np.nan)
    else:
        bastatable = funkycat.standardize(bastatable)

    if restartfile is not None:
        restartbastatable = Table.read(restartfile, format='ascii')
        restartbastatable.rename_column('starid', 'TMASS_ID')
        bastatable = join(bastatable, restartbastatable, join_type='outer')

    return bastatable


def make_sigmatable(outputtable,
                    resultsdir, errfile, warnfile,
                    idcol='TMASS_ID', floatfill=-9999.0):
    """
    This function makes an overview of warnings and error in BASTA.
    It inputs the outputtable of BASTA (the output of `make_bastatable`)
    along with the .warn and .err files and outputs an astropy Table

    Parameters
    ----------
    outputtable : astropy Table
        Table of BASTA results from run and restart run.
    resultsdir : str
        Path to directory containing `errfiles` and `warnfiles`.
    errfile : str
        Name of errfile for start and restart runs.
    warnfile : str
        Name of warningfile for start and restart runs.
    idcol : str
        Name of ID column in abs`outputtable`.
    floatfill : float
        Value of fillvalue for `outputtable`.

    Returns
    -------
    outputtable : astropy Table
        eoutputtable` with added columns providing an overview
        of errors and warnings with the same len as `outputtable`.
    """
    errstars = []
    errs = []
    warnstars = []
    warns = []
    sigmas = []
    sigmacols = []

    errfile = os.path.join(resultsdir, errfile)
    with open(errfile) as fp:
        read = csv.reader(fp, delimiter='\t')
        for line in read:
            errstars.append(line[0])
            errs.append(line[1])

    warnfile = os.path.join(resultsdir, warnfile)
    with open(warnfile) as fp:
        read = csv.reader(fp, delimiter='\t')
        for line in read:
            warnstars.append(line[0])
            warns.append(line[1])
            sigmas.append(line[2])

    assert len(errstars) == len(errs)
    assert len(warnstars) == len(warns) == len(sigmas)

    errstars = np.asarray(errstars)
    errs = np.asarray(errs)
    warnstars = np.asarray(warnstars)
    warns = np.asarray(warns)
    sigmas = np.asarray(sigmas)

    # Add TMASS_ID column
    sigmacols.append(outputtable[idcol])

    # Make binary error column
    errors = np.zeros(len(outputtable[idcol]))
    wmessages = ['No models found', 'Joint distance']
    othermask = np.ones(len(errs), dtype=bool)
    for i, wm in enumerate(wmessages):
        messagemask = np.flatnonzero(np.core.defchararray.find(errs, wm)!=-1)
        mstarmask = np.isin(outputtable[idcol], errstars[messagemask])
        errors[mstarmask] = i + 1
        othermask[messagemask] = False

    # Take other errors into account
    omask = np.isin(outputtable[idcol], errstars[othermask])
    errors[omask] = 1
    errorcol = Column(errors, name='ERRORS_BASTA')
    sigmacols.append(errorcol)

    # Find the names of the warnings
    totalwarns = []
    noofwarns = []
    for w in warns:
        w = w.replace("[",'')
        w = w.replace("]",'')
        w = w.replace("'",'')
        w = w.replace(" ",'')
        ws = np.asarray(w.split(','))
        for i in ws:
            totalwarns.append(i)
        noofwarns.append(len(ws))
    totalwarns = np.asarray(totalwarns)
    noofwarns = np.asarray(noofwarns)

    # Make count of number of warnings columns
    noofwarncol = np.zeros(len(outputtable[idcol]))
    warnmask = np.isin(outputtable[idcol], warnstars)
    assert len(noofwarns) == np.sum(warnmask)
    noofwarncol[warnmask] = noofwarns
    noofwarncol = Column(noofwarncol, name='WARNINGS_BASTA')
    sigmacols.append(noofwarncol)

    # Construct sigma warning columns
    warnnames = np.unique(totalwarns)
    for p in warnnames:
        xcol = np.ones(len(outputtable[idcol])) * floatfill
        for wstar, w, sigma in zip(warnstars, warns, sigmas):
            if p in w:
                linew = w.replace('[','').replace(']','').replace(' ','').replace("'",'').split(',')
                idx = linew.index(p)
                lines = np.asarray(sigma.replace('[','').replace(']','').replace(' ','').replace("'",'').split(','))
                lines = lines[idx].astype(float)
                widx = [cid == str(wstar) for cid in outputtable[idcol]]
                xcol[widx] = lines
        xcol = Column(np.asarray(xcol), name='FLAG_' + p.upper() + '_BASTA')
        sigmacols.append(xcol)

    # Construct sigma table from columns
    sigmatable = Table(sigmacols)
    outputtable = join(outputtable, sigmatable, keys=idcol)

    return outputtable, totalwarns


def get_errorwarning_overview(idstr, outputtable, plotsname,
                              resultsdir, errfile, warnfile,
                              floatfill=-9999.0):
    """
    Merge BASTA warnings and errors into skycat
    """
    plotsname += '_' + idstr
    outputtable, totalwarns = make_sigmatable(outputtable,
                                              resultsdir=resultsdir,
                                              errfile=errfile,
                                              warnfile=warnfile)

    # Text overview
    flags = [col for col in outputtable.columns
             if col.startswith('FLAG_') and col.endswith('_BASTA')]
    if len(flags) == 0:
        return outputtable
    for wc in [0, 1, 2, 3, 4]:
        wcmask = (outputtable['WARNINGS_BASTA'] == wc)
        print('%g stars have %g warnings' % (np.sum(wcmask), wc))
        for flag in flags:
            flagmask = (outputtable[flag][wcmask] != floatfill)
            print('%g stars raise %s' % (np.sum(flagmask), flag))

        print('%g of them also cast an error' % (
            np.sum(outputtable['ERRORS_BASTA'][wcmask] > 0)))
        for e in np.unique(outputtable['ERRORS_BASTA'][wcmask]):
            if e == 0:
                continue
            print('%g of them cast error %g' % (
                np.sum(outputtable['ERRORS_BASTA'][wcmask] == e), e))
        print('')

    draw.hist(plotsname,
              'Warnings',
              totalwarns)

    draw.corner(plotsname, flags,
                [outputtable[flag] for flag in flags])

    for flag in flags:
        draw.hist(plotsname, flag, outputtable[flag])

    return outputtable
