# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# amalie
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###############################################################################
# MODULES
###############################################################################
import os
import numpy as np
from xml.etree import ElementTree
from astropy.table import Column
import astropy.units as u
import astropy.coordinates as coord
from galpy.orbit import Orbit
from galpy.actionAngle import actionAngleStaeckel, estimateDeltaStaeckel
from galpy.util import coords
from galpy.util.conversion import get_physical

from cattoys import toolcat

# Don't print Astropy warnings
import warnings
from astropy.utils.exceptions import AstropyWarning
warnings.filterwarnings('ignore', category=AstropyWarning)

from galpy.potential.mwpotentials import McMillan17 as mcm
from galpy.potential.mwpotentials import MWPotential2014 as mw

""" NOTE ON POTENTIAL
Galpys MWPotential2014 is an axisymmetric potential fit to Milky Way data
constructured from a NFW halo, a Niyamota-Nagai disc and a power sphere bulge

We can construct out own Milky Way like potential by combining different
components.
Note that the normalize values add up to 1, such that the circular velocity
will be 1 at R=1.
Example from summer school:
    from galpy.potential import MiyamotoNagaiPotential,\
            NFWPotential, HernquistPotential
    mp= MiyamotoNagaiPotential(a=0.5,b=0.0375,normalize=.6)
    nfp= NFWPotential(a=4.5,normalize=.35)
    hp= HernquistPotential(a=0.6/8,normalize=0.05)
    pot=[]
    pot.append(mp)
    pot.append(nfp)
    pot.append(hp)

We can also add non-axisymmetric, time varying components such as a bar or
spiral structures.
The bar is grown smoothly from tform=0 (in bar periods) for tsteady=5 bar
periods unsing in built functionality.
The spiral arms must be grown using the DehnenSmoothWrapperPotential where
we use the same formation time of the bar: dp.tform()
Example from summer school:
    from galpy.potential import DehnenBarPotential, SpiralArmsPotential,
        DehnenSmoothWrapperPotential
    dp=DehnenBarPotential(omegab=1.3,rb=5./8.,Af=(2./75.),
        tform=0,tsteady=5,barphi=25/180.*numpy.pi)
    sp=DehnenSmoothWrapperPotential(
        pot=SpiralArmsPotential(N=4,amp=1.,phi_ref=np.pi/4.,
        alpha=np.deg2rad(12.),omega=0.91),tform=dp.tform(),tsteady=5)
    pot.append(dp)
    pot.append(sp)
    o2=Orbit(vxvv=[0.5,0.1,1.1,0.,0.1,0.])
    o2.integrate(ts,pot,method='odeint')
    o2.plot()
    o2.plot(d1='x',d2='y')
    o2.plot(d1='x',d2='z')
"""


def make_sigmamatrix(ra_e, dec_e, plx_e, pmra_e, pmdec_e, vlos_e,
                     ra_dec_cor, ra_plx_cor, ra_pmra_cor, ra_pmdec_cor,
                     dec_plx_cor, dec_pmra_cor, dec_pmdec_cor, plx_pmra_cor,
                     plx_pmdec_cor, pmra_pmdec_cor):
    """
    This function outputs the covariance matrix for the dynamics computation.

    Parameters
    ----------
    ra_e : float
        Uncertainty in right ascension in degrees
    dec_e : float
        Uncertainty in declination in degrees
    pmra_e : float
        Uncertainty in proper motion in  as/yr
    pmdec_e : float
        Uncertainty in proper motion in  as/yr
    dist_e : float
        Uncertainty in distance to the star in parsec
    vlos_e : float
        Uncertainty in line-of-sight velocity of star in km/s
    ra_dec_cor : float
        Correction between right ascension and declination
    ra_plx_cor : float
        Correction between right ascension and parallax
    ra_pmra_cor : float
        Correction between right ascension and proper motion in right ascension
    ra_pmdec_cor : float
        Correction between right ascension and proper motion in declination
    dec_plx_cor : float
        Correction between declination and parallax
    dec_pmra_cor : float
        Correction between declination and proper motion in right ascension
    dec_pmdec_cor : float
        Correction between declination and proper motion in declination
    plx_pmra_cor : float
        Correction between parallax and proper motion in right ascension
    plx_pmdec_cor : float
        Correction between parallax and proper motion in declination
    pmra_pmdec_cor : float
        Correction between proper motion in right ascension and proper motion
        in declination

    Returns
    -------
    sigma : array
        Covariance matrix
    """
    ra_e = toolcat.convert_angles(ra_e)
    dec_e = toolcat.convert_angles(dec_e)

    # Build entries
    s00 = ra_e ** 2
    s11 = dec_e ** 2
    s22 = plx_e ** 2
    s33 = pmra_e ** 2
    s44 = pmdec_e ** 2
    s55 = vlos_e ** 2

    s01 = ra_e * dec_e * ra_dec_cor
    s02 = ra_e * plx_e * ra_plx_cor
    s03 = ra_e * pmra_e * ra_pmra_cor
    s04 = ra_e * pmdec_e * ra_pmdec_cor
    s05 = 0

    s12 = dec_e * plx_e * dec_plx_cor
    s13 = dec_e * pmra_e * dec_pmra_cor
    s14 = dec_e * pmdec_e * dec_pmdec_cor
    s15 = 0

    s23 = plx_e * pmra_e * plx_pmra_cor
    s24 = plx_e * pmdec_e * plx_pmdec_cor
    s25 = 0

    s34 = pmra_e * pmdec_e * pmra_pmdec_cor
    s35 = 0

    s45 = 0

    sigma = np.array([
        [s00, s01, s02, s03, s04, s05],
        [s01, s11, s12, s13, s14, s15],
        [s02, s12, s22, s23, s24, s25],
        [s03, s13, s23, s33, s34, s35],
        [s04, s14, s24, s34, s44, s45],
        [s05, s15, s25, s35, s45, s55]
    ])
    return sigma


def sample_gaiaphase(mu, ra_e, dec_e, plx_e, pmra_e, pmdec_e, vlos_e,
                     ra_dec_cor, ra_plx_cor, ra_pmra_cor, ra_pmdec_cor,
                     dec_plx_cor, dec_pmra_cor, dec_pmdec_cor, plx_pmra_cor,
                     plx_pmdec_cor, pmra_pmdec_cor, n_sample=10000):
    """
    This function returns a sample, useful for numerically computing
    uncertainties in phase-space.

    Parameters
    ----------
    mu : array
        Array containing the observables [ra, dec, plx, pmra, pmdec, vlos]
    ra_e : float
        Uncertainty in right ascension in degrees
    dec_e : float
        Uncertainty in declination in degrees
    pmra_e : float
        Uncertainty in proper motion in  as/yr
    pmdec_e : float
        Uncertainty in proper motion in  as/yr
    dist_e : float
        Uncertainty in distance to the star in parsec
    vlos_e : float
        Uncertainty in line-of-sight velocity of star in km/s
    ra_dec_cor : float
        Correction between right ascension and declination
    ra_plx_cor : float
        Correction between right ascension and parallax
    ra_pmra_cor : float
        Correction between right ascension and proper motion in right ascension
    ra_pmdec_cor : float
        Correction between right ascension and proper motion in declination
    dec_plx_cor : float
        Correction between declination and parallax
    dec_pmra_cor : float
        Correction between declination and proper motion in right ascension
    dec_pmdec_cor : float
        Correction between declination and proper motion in declination
    plx_pmra_cor : float
        Correction between parallax and proper motion in right ascension
    plx_pmdec_cor : float
        Correction between parallax and proper motion in declination
    pmra_pmdec_cor : float
        Correction between proper motion in right ascension and proper motion
        in declination
    n_sample : int, optional
        Number of drawn samples from distribution.
        Default is 10000.

    Returns
    -------
    sample : array
        a 6 x n_sample array of the drawn samples.
    """
    # Get sigma
    sigma = make_sigmamatrix(ra_e, dec_e, plx_e, pmra_e, pmdec_e, vlos_e,
                             ra_dec_cor, ra_plx_cor, ra_pmra_cor, ra_pmdec_cor,
                             dec_plx_cor, dec_pmra_cor, dec_pmdec_cor,
                             plx_pmra_cor, plx_pmdec_cor, pmra_pmdec_cor)

    # Sample from distibution
    sample = np.random.multivariate_normal(mu, sigma, size=n_sample)
    return sample


def get_dynamics(kitten, get_orbit=False, physical=True, n_sample=10000,
                 gaiarv=True, useplx=False, bastaplx=True, bailerjones=False,
                 bastaresultdir=None, xmlfile=None, losvcol=False, distancecol=None,
                 mcmillan=False):
    """
    This function computes kinematics parameters for a given sample of stars.

    Parameters
    ----------
    kitten : astropy Table
        The catalogue of the sample of stars containing columns from Gaia
        along with at least one measure of distance.
    get_orbit : bool, optional
        Flag determining whether to compute orbit. Nothing is currently saved
        from this computation. Default is False.
    physical : bool, optional
        Flag determining whether the actions/angles/frequencies should have
        physical units or scaled units. Default is False.
    n_sample : int, optional
        Number of samples drawn in the numerical uncertainty computation.
        This value is given as a keyword argument to sample_gaiaphase.
        Default is 10000.
    gaiarv : bool, optional
        Flag determining whether to use line-of-sight velocities from
        Gaia (True) or APOGEE (False). Default is True.
    useplx : bool, optional
        Flag determining whether to use inverse parallax as distance.
        Default is False.
    bastaplx : bool, optional
        Flag determining whether to use distances from BASTA where parallax
        was included in the set of fitting parameters.
        Default is True.
    bailerjones : bool, optional
        Flag determining whether to use distances from Bailer-Jones+2018.
        Default is False.

    Returns
    -------
    kitten : astropy Table
        An updated kitten including new columns containing computed
        kinematic and dymanic quantities and their (numerical) uncertainties.
    """
    print('Get dynamics')

    # First let's define the characteristica of our Galaxy.
    # R0 is the distance between the Sun and the Galactic plane
    # V0 is the circular velocity at the distance of the Sun.
    # It is important to note that the galcen_distance and R0 are not
    # identical as the former is the distance between the Sun and the GC,
    # while the latter is the projection of this distance onto the
    # Galactic midplane.
    # However, using geometry we can see that due to the Sun being almost at
    # the Galactic midplane, the difference is small.
    #R0 = 8.34 * u.kpc  # Reid et al 2014
    #galcen_distance = 8.3 * u.kpc
    #V0 = 220 * u.km/u.s  # Bovy 2015
    #z_sun = 27 * u.pc  # Chen et al 2001
    """
    R0 = 8 * u.kpc  # Reid et al 2014
    galcen_distance = 8 * u.kpc
    V0 = 220 * u.km/u.s  # Bovy 2015
    z_sun = 25 * u.pc  # Juric et al 2008 (SDSS)
    """
    #R0 = 8.2 * u.kpc  # Gravity Collaboration+ 2019
    #V0 = 240 * u.km/u.s  # Bovy 2015

    # Potential
    if mcmillan:
        pot = mcm
    else:
        pot = mw

    # Velocity of the Sun w.r.t the Local Standard of Rest (Schonrich+ 2010)
    U_LSR = 11.1 * u.km/u.s
    V_LSR = 12.24 * u.km/u.s
    W_LSR = 7.25 * u.km/u.s

    z_sun = 20.8 * u.pc  # Bennett & Bovy 2019
    R0 = get_physical(pot)['ro'] * u.kpc
    V0 = get_physical(pot)['vo'] * u.km/u.s - V_LSR
    galcen_distance = np.sqrt(R0 ** 2 + z_sun ** 2)


    # Galactocentric velocity on the Sun
    v_sun_GC = coord.CartesianDifferential([U_LSR, V0+V_LSR, W_LSR])
    # Another important thing to note here is that the Galactocentric frame
    # in astropy is right-handed, while galpy uses a left-handed frame.
    # This changes the sign of the x-component of galcen_v_sun=[-U, V, W]
    # in galpy instead of galcen_v_sun=[U, V, W].
    # In right-handed system, the x-axis is positive towards the Galactic
    # center, meaning v_x is opposite of the Galactocentric radial velocity.

    # Define Galactocentric frame
    GC = coord.Galactocentric(galcen_distance=galcen_distance,
                              galcen_v_sun=v_sun_GC,
                              z_sun=z_sun)

    # Unpack astrometric observables
    if losvcol:
        vloss = kitten[losvcol].data  # * u.km/u.s
        vloss_e = kitten[losvcol].data  # * u.km/u.s
    else:
        vloss = kitten['LOS_VELOCITY_GAIA'].data  # * u.km/u.s
        vloss_e = kitten['ERROR_LOS_VELOCITY_GAIA'].data  # * u.km/u.s

    if distancecol is not None:
        dists = kitten[distancecol].data  # * u.pc
        errp = kitten['UPPER_ERROR_' + distancecol].data
        errm = kitten['LOWER_ERROR_' + distancecol].data
        dists_e = np.maximum(errp, errm)
    elif useplx:
        # dists = coord.Distance(parallax=kitten['PARALLAX_GAIA'].data * u.mas,
        #                       allow_negative=True)
        # dists_e = coord.Distance(parallax=kitten['ERROR_PARALLAX_GAIA'].data * u.mas,
        #                       allow_negative=True)
        # dists = dists.value
        # dists_e = dists.value
        dists = kitten['PARALLAX_GAIA'].data * 1e-3
        dists_e = kitten['ERROR_PARALLAX_GAIA'].data * 1e-3
        dists = (1 / dists)  # * u.pc
        dists_e = (1 / dists_e)  # * u.pc
    else:
        if bastaplx:
            dists = kitten['DISTANCE_BASTAPLX'].data  # * u.pc
            errp = kitten['UPPER_ERROR_DISTANCE_BASTAPLX'].data
            errm = kitten['LOWER_ERROR_DISTANCE_BASTAPLX'].data
        elif bailerjones:
            dists = kitten['DISTANCE_BAILERJONES'].data
            errp = kitten['UPPER_ERROR_DISTANCE_BAILERJONES'].data - dists
            errm = dists - kitten['LOWER_ERROR_DISTANCE_BAILERJONES'].data
        else:
            dists = kitten['DISTANCE_BASTA'].data  # * u.pc
            errp = kitten['UPPER_ERROR_DISTANCE_BASTA'].data
            errm = kitten['LOWER_ERROR_DISTANCE_BASTA'].data
        dists_e = np.maximum(errp, errm)

    ras = kitten['RA_GAIA'].data  # * u.deg
    ras_e = kitten['ERROR_RA_GAIA'].data  # * u.mas
    decs = kitten['DEC_GAIA'].data  # * u.deg
    decs_e = kitten['ERROR_DEC_GAIA'].data  # * u.mas
    plxs = kitten['UNCORRECTED_PARALLAX_GAIA'].data  # * u.mas
    plxs_e = kitten['ERROR_PARALLAX_GAIA'].data  # * u.mas
    pmracosdecs = kitten['PMRA_GAIA'].data  # * u.mas / u.yr
    pmracosdecs_e = kitten['ERROR_PMRA_GAIA'].data  # * u.mas / u.yr
    pmdecs = kitten['PMDEC_GAIA'].data  # * u.mas / u.yr
    pmdecs_e = kitten['ERROR_PMDEC_GAIA'].data  # * u.mas / u.yr
    ra_dec_cors = kitten['RA_DEC_CORR_GAIA'].data
    ra_plx_cors = kitten['RA_PARALLAX_CORR_GAIA'].data
    ra_pmra_cors = kitten['RA_PMRA_CORR_GAIA'].data
    ra_pmdec_cors = kitten['RA_PMDEC_CORR_GAIA'].data
    dec_plx_cors = kitten['DEC_PARALLAX_CORR_GAIA'].data
    dec_pmra_cors = kitten['DEC_PMRA_CORR_GAIA'].data
    dec_pmdec_cors = kitten['DEC_PMDEC_CORR_GAIA'].data
    plx_pmra_cors = kitten['PARALLAX_PMRA_CORR_GAIA'].data
    plx_pmdec_cors = kitten['PARALLAX_PMDEC_CORR_GAIA'].data
    pmra_pmdec_cors = kitten['PMRA_PMDEC_CORR_GAIA'].data

    tmassids = kitten['TMASS_ID']

    # Allocate memory
    medxgc = np.empty(len(ras))
    stdm_xgc = np.empty(len(ras))
    stdp_xgc = np.empty(len(ras))
    medygc = np.empty(len(ras))
    stdm_ygc = np.empty(len(ras))
    stdp_ygc = np.empty(len(ras))
    medzgc = np.empty(len(ras))
    stdm_zgc = np.empty(len(ras))
    stdp_zgc = np.empty(len(ras))
    medxhc = np.empty(len(ras))
    stdm_xhc = np.empty(len(ras))
    stdp_xhc = np.empty(len(ras))
    medyhc = np.empty(len(ras))
    stdm_yhc = np.empty(len(ras))
    stdp_yhc = np.empty(len(ras))
    medzhc = np.empty(len(ras))
    stdm_zhc = np.empty(len(ras))
    stdp_zhc = np.empty(len(ras))
    medu = np.empty(len(ras))
    stdm_u = np.empty(len(ras))
    stdp_u = np.empty(len(ras))
    medv = np.empty(len(ras))
    stdm_v = np.empty(len(ras))
    stdp_v = np.empty(len(ras))
    medw = np.empty(len(ras))
    stdm_w = np.empty(len(ras))
    stdp_w = np.empty(len(ras))
    medR = np.empty(len(ras))
    stdm_R = np.empty(len(ras))
    stdp_R = np.empty(len(ras))
    medphi = np.empty(len(ras))
    stdm_phi = np.empty(len(ras))
    stdp_phi = np.empty(len(ras))
    medz = np.empty(len(ras))
    stdm_z = np.empty(len(ras))
    stdp_z = np.empty(len(ras))
    medvR = np.empty(len(ras))
    stdm_vR = np.empty(len(ras))
    stdp_vR = np.empty(len(ras))
    medvphi = np.empty(len(ras))
    stdm_vphi = np.empty(len(ras))
    stdp_vphi = np.empty(len(ras))
    medvz = np.empty(len(ras))
    stdm_vz = np.empty(len(ras))
    stdp_vz = np.empty(len(ras))
    medjR = np.empty(len(ras))
    stdm_jR = np.empty(len(ras))
    stdp_jR = np.empty(len(ras))
    medlz = np.empty(len(ras))
    stdm_lz = np.empty(len(ras))
    stdp_lz = np.empty(len(ras))
    medjz = np.empty(len(ras))
    stdm_jz = np.empty(len(ras))
    stdp_jz = np.empty(len(ras))
    medOR = np.empty(len(ras))
    stdm_OR = np.empty(len(ras))
    stdp_OR = np.empty(len(ras))
    medOphi = np.empty(len(ras))
    stdm_Ophi = np.empty(len(ras))
    stdp_Ophi = np.empty(len(ras))
    medOz = np.empty(len(ras))
    stdm_Oz = np.empty(len(ras))
    stdp_Oz = np.empty(len(ras))
    medtR = np.empty(len(ras))
    stdm_tR = np.empty(len(ras))
    stdp_tR = np.empty(len(ras))
    medtphi = np.empty(len(ras))
    stdm_tphi = np.empty(len(ras))
    stdp_tphi = np.empty(len(ras))
    medtz = np.empty(len(ras))
    stdm_tz = np.empty(len(ras))
    stdp_tz = np.empty(len(ras))
    mede = np.empty(len(ras))
    stdm_e = np.empty(len(ras))
    stdp_e = np.empty(len(ras))
    medzmax = np.empty(len(ras))
    stdm_zmax = np.empty(len(ras))
    stdp_zmax = np.empty(len(ras))
    medrperi = np.empty(len(ras))
    stdm_rperi = np.empty(len(ras))
    stdp_rperi = np.empty(len(ras))
    medrap = np.empty(len(ras))
    stdm_rap = np.empty(len(ras))
    stdp_rap = np.empty(len(ras))
    medrguide = np.empty(len(ras))
    stdm_rguide = np.empty(len(ras))
    stdp_rguide = np.empty(len(ras))
    medenergy = np.empty(len(ras))
    stdm_energy = np.empty(len(ras))
    stdp_energy = np.empty(len(ras))
    medLX = np.empty(len(ras))
    stdm_LX = np.empty(len(ras))
    stdp_LX = np.empty(len(ras))
    medLY = np.empty(len(ras))
    stdm_LY = np.empty(len(ras))
    stdp_LY = np.empty(len(ras))
    medLZ = np.empty(len(ras))
    stdm_LZ = np.empty(len(ras))
    stdp_LZ = np.empty(len(ras))
    medLTOT = np.empty(len(ras))
    stdm_LTOT = np.empty(len(ras))
    stdp_LTOT = np.empty(len(ras))

    for i, (ra, ra_e, dec, dec_e, plx, plx_e, pmra, pmra_e, pmdec, pmdec_e,
            vlos, vlos_e, ra_dec_cor, ra_plx_cor, ra_pmra_cor, ra_pmdec_cor,
            dec_plx_cor, dec_pmra_cor, dec_pmdec_cor, plx_pmra_cor,
            plx_pmdec_cor, pmra_pmdec_cor, dist, dist_e, tmassid) in enumerate(zip(
                ras, ras_e, decs, decs_e, plxs, plxs_e,
                pmracosdecs, pmracosdecs_e, pmdecs, pmdecs_e,
                vloss, vloss_e,
                ra_dec_cors, ra_plx_cors, ra_pmra_cors, ra_pmdec_cors,
                dec_plx_cors, dec_pmra_cors, dec_pmdec_cors,
                plx_pmra_cors, plx_pmdec_cors, pmra_pmdec_cors,
                dists, dists_e, tmassids)):
        if ((i % 200) == 0):
            print('Sample %s out of %s stars' % (i, len(ras)))

        # Sample and get uncertainties
        mu = np.array([ra, dec, plx, pmra, pmdec, vlos])
        sample = sample_gaiaphase(mu,
                                  ra_e, dec_e, plx_e, pmra_e, pmdec_e, vlos_e,
                                  ra_dec_cor, ra_plx_cor, ra_pmra_cor,
                                  ra_pmdec_cor, dec_plx_cor, dec_pmra_cor,
                                  dec_pmdec_cor, plx_pmra_cor, plx_pmdec_cor,
                                  pmra_pmdec_cor, n_sample=n_sample)

        if useplx:
            dists_sample = coord.Distance(parallax=sample[:, 2] * u.mas,
                                          allow_negative=True)
        elif bastaresultdir:
            import bastatools.jsontool as jt
            assert os.path.isdir(bastaresultdir)
            assert xmlfile is not None
            jsonfile = os.path.join(bastaresultdir, tmassid + '.json')
            assert os.path.isfile(jsonfile)

            if not os.path.exists(xmlfile):
                raise FileNotFoundError('Input file not found!')
            oldpath = os.getcwd()
            xmlpath = '/'.join(xmlfile.split('/')[:-1])
            xmlname = xmlfile.split('/')[-1]
            if xmlpath:
                os.chdir(xmlpath)

            # Parse XML file
            tree = ElementTree.parse(xmlname)
            root = tree.getroot()
            gridfile = root.find('default/library').get('path')

            vs, ps = jt.get_likelihoods(jsonfile, '', gridfile,
                                        ['distance_joint'],
                                        xmlfile=xmlfile, save=False)
            vs = vs[0]
            ps = ps[0]
            dists_sample = np.random.choice(vs, p=ps/np.sum(ps), size=n_sample) * u.pc
        else:
            dists_sample = np.random.normal(dist, dist_e, n_sample) * u.pc
            dists_sample[dists_sample <= 0] = 0.01 * u.pc

        # Then we get the orbit from galpy
        css = coord.SkyCoord(ra=sample[:, 0] * u.deg,
                             dec=sample[:, 1] * u.deg,
                             distance=dists_sample,
                             pm_ra_cosdec=sample[:, 3] * u.mas / u.year,
                             pm_dec=sample[:, 4] * u.mas / u.year,
                             radial_velocity=sample[:, 5] * u.km / u.s,
                             galcen_distance=galcen_distance,
                             galcen_v_sun=v_sun_GC,
                             z_sun=z_sun)

        o = Orbit(css, **get_physical(pot))
        energies = o.E(analytic=True, type='staeckel', pot=pot, use_physical=True)
        L = o.L(analytic=True, type='staeckel', pot=pot, use_physical=True)
        LX = L[:, 0]
        LY = L[:, 1]
        LZ = L[:, 2]

        LTOT = np.sqrt(LX ** 2 + LY ** 2 + LZ ** 2)

        star = coord.SkyCoord(ra=sample[:, 0] * u.deg,
                              dec=sample[:, 1] * u.deg,
                              distance=dists_sample,
                              pm_ra_cosdec=sample[:, 3] * u.mas / u.year,
                              pm_dec=sample[:, 4] * u.mas / u.year,
                              radial_velocity=sample[:, 5] * u.km / u.s)

        o = Orbit(star, **get_physical(pot))

        if get_orbit:
            if bastaplx:
                age = kitten['AGE_BASTAPLX'].data[i].astype(float) * u.Myr
            else:
                age = kitten['AGE_BASTA'].data[i].astype(float) * u.Myr
            # How many instances should be saved?
            intsteps = 1000
            ts = np.linspace(0, -age, intsteps)
            o.integrate(ts, pot)

        # Coordinates + velocities in cartesian, Heliocentric frame
        star_HC = star.transform_to(coord.Galactic)
        star_HC.representation_type = 'cartesian'
        x_HC = star_HC.u.to(u.kpc)
        y_HC = star_HC.v.to(u.kpc)
        z_HC = star_HC.w.to(u.kpc)
        u_HC = star_HC.U.to(u.km/u.s)
        v_HC = star_HC.V.to(u.km/u.s)
        w_HC = star_HC.W.to(u.km/u.s)

        # Convert to Galactocentric frame
        star = star.transform_to(GC)

        # Save coordinates in Galactocentric cartesian coordinates
        # The sign of x_GC is changed in order to be in a left-handed frame.
        x_GC = -star.x.to(u.kpc)
        y_GC = star.y.to(u.kpc)
        z_GC = star.z.to(u.kpc)

        # Convert to cylindrical Galactocentric coordinates
        star.set_representation_cls(coord.CylindricalRepresentation,
                                    s=coord.CylindricalDifferential)
        R = star.rho.to(u.kpc)
        phi = star.phi.to(u.rad)
        z = star.z.to(u.kpc)
        vR = star.d_rho.to(u.km/u.s)
        # Notice the change of sign here, converting to left-handed
        # If this is not done, the sign will be wrong in action+angle.
        vphi = -(star.d_phi.to(u.rad/u.s) * star.rho.to(u.km) / (1.*u.rad))
        vz = star.d_z.to(u.km/u.s)

        # Determine actions/angles ect. from the Staeckel approximation
        # A choice of the focal lenght delta is required for this method.
        # One could do it per star like so
        # deltas = estimateDeltaStaeckel(pot, rho, z, no_median=False)
        # Or use the median of the deltas for all stars
        aAS = actionAngleStaeckel(
            pot=pot,
            delta=0.45,  # deltas,
            c=True,
            r0=R0,
            v0=V0
        )

        jR, lz, jz, OR, Ophi, Oz, tR, tphi, tz = aAS.actionsFreqsAngles(
            R, vR, vphi, z, vz, phi, c=True)

        # A proper determination of guiding centre radii would require the
        # integration of the stellar orbits within an assummed potential
        # However, under the assumption of a flat rotation curve for the
        # Galaxy, we can approximate R_guide as
        rguide = ((lz * R0 * V0) / V0)
        # Here we assumme the circular velocity to be that of the Sun.

        # galpy ouputs the actions and frequencies in its internal coordinates
        # where distances in units of R0 and V0 so if we want the quantities
        # in physical units, we need to scale them back
        action_unit = R0.value * V0.value
        freq_unit = (1 / R0.value) * V0.value
        if physical:
            jR *= action_unit
            lz *= action_unit
            jz *= action_unit
            OR *= freq_unit
            Ophi *= freq_unit
            Oz *= freq_unit
        tR = tR * u.rad
        tphi = tphi * u.rad
        tz = tz * u.rad

        # Restrict the azimuthal angle to be within 0-2pi.
        #tphi[tphi > np.pi] -= 2 * np.pi

        # Compute orbital parameters such as eccentricity and zmax from Staeckel
        # approximation
        e, zmax, rperi, rap = aAS.EccZmaxRperiRap(R, vR, vphi, z, vz, phi)

        # Compute quantiles
        medxgc[i], stdm_xgc[i], stdp_xgc[i] = toolcat.compute_quantiles(x_GC.value)
        medygc[i], stdm_ygc[i], stdp_ygc[i] = toolcat.compute_quantiles(y_GC.value)
        medzgc[i], stdm_zgc[i], stdp_zgc[i] = toolcat.compute_quantiles(z_GC.value)
        medxhc[i], stdm_xhc[i], stdp_xhc[i] = toolcat.compute_quantiles(x_HC.value)
        medyhc[i], stdm_yhc[i], stdp_yhc[i] = toolcat.compute_quantiles(y_HC.value)
        medzhc[i], stdm_zhc[i], stdp_zhc[i] = toolcat.compute_quantiles(z_HC.value)
        medu[i], stdm_u[i], stdp_u[i] = toolcat.compute_quantiles(u_HC.value)
        medv[i], stdm_v[i], stdp_v[i] = toolcat.compute_quantiles(v_HC.value)
        medw[i], stdm_w[i], stdp_w[i] = toolcat.compute_quantiles(w_HC.value)
        medR[i], stdm_R[i], stdp_R[i] = toolcat.compute_quantiles(R.value)
        medphi[i], stdm_phi[i], stdp_phi[i] = toolcat.compute_quantiles(phi.value)
        medz[i], stdm_z[i], stdp_z[i] = toolcat.compute_quantiles(z.value)
        medvR[i], stdm_vR[i], stdp_vR[i] = toolcat.compute_quantiles(vR.value)
        medvphi[i], stdm_vphi[i], stdp_vphi[i] = toolcat.compute_quantiles(vphi.value)
        medvz[i], stdm_vz[i], stdp_vz[i] = toolcat.compute_quantiles(vz.value)
        medjR[i], stdm_jR[i], stdp_jR[i] = toolcat.compute_quantiles(jR)
        medlz[i], stdm_lz[i], stdp_lz[i] = toolcat.compute_quantiles(lz)
        medjz[i], stdm_jz[i], stdp_jz[i] = toolcat.compute_quantiles(jz)
        medOR[i], stdm_OR[i], stdp_OR[i] = toolcat.compute_quantiles(OR)
        medOphi[i], stdm_Ophi[i], stdp_Ophi[i] = toolcat.compute_quantiles(Ophi)
        medOz[i], stdm_Oz[i], stdp_Oz[i] = toolcat.compute_quantiles(Oz)
        medtR[i], stdm_tR[i], stdp_tR[i] = toolcat.compute_quantiles(tR.value)
        medtphi[i], stdm_tphi[i], stdp_tphi[i] = toolcat.compute_quantiles(tphi.value)
        medtz[i], stdm_tz[i], stdp_tz[i] = toolcat.compute_quantiles(tz.value)
        mede[i], stdm_e[i], stdp_e[i] = toolcat.compute_quantiles(e)
        medzmax[i], stdm_zmax[i], stdp_zmax[i] = toolcat.compute_quantiles(zmax)
        medrperi[i], stdm_rperi[i], stdp_rperi[i] = toolcat.compute_quantiles(rperi)
        medrap[i], stdm_rap[i], stdp_rap[i] = toolcat.compute_quantiles(rap)
        medrguide[i], stdm_rguide[i], stdp_rguide[i] = toolcat.compute_quantiles(rguide.value)
        medenergy[i], stdm_energy[i], stdp_energy[i] = toolcat.compute_quantiles(energies)
        medLX[i], stdm_LX[i], stdp_LX[i] = toolcat.compute_quantiles(LX)
        medLY[i], stdm_LY[i], stdp_LY[i] = toolcat.compute_quantiles(LY)
        medLZ[i], stdm_LZ[i], stdp_LZ[i] = toolcat.compute_quantiles(LZ)
        medLTOT[i], stdm_LTOT[i], stdp_LTOT[i] = toolcat.compute_quantiles(LTOT)


    # Save it in cat
    meds = [medxgc, medygc, medzgc,
            medxhc, medyhc, medzhc, medu, medv, medw,
            medR, medphi, medz, medvR, medvphi, medvz,
            medjR, medlz, medjz, medOR, medOphi, medOz,
            medtR, medtphi, medtz, mede, medzmax, medrperi, medrap, medrguide, medenergy,
            medLX, medLY, medLZ, medLTOT]
    ps = [stdp_xgc, stdp_ygc, stdp_zgc,
          stdp_xhc, stdp_yhc, stdp_zhc, stdp_u, stdp_v, stdp_w,
          stdp_R, stdp_phi, stdp_z, stdp_vR, stdp_vphi, stdp_vz,
          stdp_jR, stdp_lz, stdp_jz, stdp_OR, stdp_Ophi, stdp_Oz,
          stdp_tR, stdp_tphi, stdp_tz, stdp_e, stdp_zmax,
          stdp_rperi, stdp_rap, stdp_rguide, stdp_energy,
          stdp_LX, stdp_LY, stdp_LZ, stdp_LTOT]
    ms = [stdm_xgc, stdm_ygc, stdm_zgc,
          stdm_xhc, stdm_yhc, stdm_zhc, stdm_u, stdm_v, stdm_w,
          stdm_R, stdm_phi, stdm_z, stdm_vR, stdm_vphi, stdm_vz,
          stdm_jR, stdm_lz, stdm_jz, stdm_OR, stdm_Ophi, stdm_Oz,
          stdm_tR, stdm_tphi, stdm_tz, stdm_e, stdm_zmax,
          stdm_rperi, stdm_rap, stdm_rguide, stdm_energy,
          stdm_LX, stdm_LY, stdm_LZ, stdm_LTOT]
    names = ['XRECT_GC', 'YRECT_GC', 'ZRECT_GC',
             'X_HC', 'Y_HC', 'Z_HC', 'U_HC', 'V_HC', 'W_HC',
             'R_GC', 'PHI_GC', 'Z_GC', 'VR_GC', 'VPHI_GC', 'VZ_GC',
             'JR', 'LZ', 'JZ', 'OR', 'OPHI', 'OZ',
             'THETA_R', 'THETA_PHI', 'THETA_Z',
             'ECCENTRICITY', 'ZMAX', 'RPERI', 'RAP', 'RGUIDE', 'ENERGY',
             'L_X', 'L_Y', 'L_Z', 'LTOT']
    for (med, p, m, name) in zip(meds, ps, ms, names):
        medcol = Column(med, name=name)
        kitten.add_column(medcol)
        pcol = Column(p, name='UPPER_ERROR_' + name)
        kitten.add_column(pcol)
        mcol = Column(m, name='LOWER_ERROR_' + name)
        kitten.add_column(mcol)

    print('Done!')


def theoretical_galacticvelocities(ra, dec, pmra, pmra_e, pmdec, pmdec_e,
                                   d, d_e, vlos, vlos_e):
    """
    This function transforms observed coordinates in ICRS to Galactic velocity
    components (heliocentric) following the approach in
    Johnson & Soderblom 1987 but using the T matrix (which depend on the
    equatorial coordinates of the Galactic North Pole in epoch J2000 and the
    position angle of the North Celestial Pole relative to the great semicircle
    passing through the NGP and zero Galactic longitude) from the Gaia
    documentation, which was also used in the Hipparcos mission:
        https://gea.esac.esa.int/archive/documentation/GDR2/Data_processing/
        chap_cu3ast/sec_cu3ast_intro/ssec_cu3ast_intro_tansforms.html

    Parameters
    ----------
    ra : value
        Right ascension in degrees
    dec : value
        Declination in degrees
    pmra : value
        Proper motion in  as/yr
    pmra_e : value
        Uncertainty in proper motion in  as/yr
    pmdec : value
        Proper motion in  as/yr
    pmdec_e : value
        Uncertainty in proper motion in  as/yr
    d : value
        Distance to the star in parsec
    d_e : value
        Uncertainty in distance to the star in parsec
    vlos : value
        Line-of-sight velocity of star in km/s
    vlos_e : value
        Uncertainty in line-of-sight velocity of star in km/s

    Returns
    -------
    u : value
        Velocity in heliocentric X
    v : value
        Velocity in heliocentric Y
    w : value
        Velocity in heliocentric Z
    stdu : value
        Unvertainty in velocity in heliocentric X
    stdv : value
        Uncertainty in velocity in heliocentric Y
    stdw : value
        Uncertainty in velocity in heliocentric Z
    """
    print('Get Galactic velocity components')
    assert (len(ra) == len(dec) == len(pmra) == len(pmdec) ==
            len(d) == len(vlos))
    u = np.zeros(len(ra))
    v = np.zeros(len(ra))
    w = np.zeros(len(ra))
    stdu = np.zeros(len(ra))
    stdv = np.zeros(len(ra))
    stdw = np.zeros(len(ra))

    k = 4.74047  # the equivalent in km/s of one AU in one tropical year

    T = [-0.0548756, -0.8734371, -0.4838350,
         +0.4941094, -0.4448296, +0.7469822,
         -0.8676661, -0.1980764, +0.4559838]
    T = np.array(T)
    T = T.reshape((3, 3))

    for i in range(len(ra)):
        A = [np.cos(np.deg2rad(ra[i])) * np.cos(np.deg2rad(dec[i])),
             -np.sin(np.deg2rad(ra[i])),
             -np.cos(np.deg2rad(ra[i])) * np.sin(np.deg2rad(dec[i])),
             np.sin(np.deg2rad(ra[i])) * np.cos(np.deg2rad(dec[i])),
             np.cos(np.deg2rad(ra[i])),
             -np.sin(np.deg2rad(ra[i])) * np.sin(np.deg2rad(dec[i])),
             np.sin(np.deg2rad(dec[i])),
             0,
             np.cos(np.deg2rad(dec[i]))]
        A = np.array(A)
        A = A.reshape((3, 3))
        X = [vlos[i], k * pmra[i] * d[i], k * pmdec[i] * d[i]]
        X = np.array(X)
        B = np.dot(T, A)
        vel = np.dot(B, X)
        assert vel.shape == (3,)
        u[i] = vel[0]
        v[i] = vel[1]
        w[i] = vel[2]

        # B describes the local rotation from ICRS to galactic in the tangent
        # plane of the celestrial sphere
        p = [vlos_e[i] ** 2,
             ((k * d[i]) ** 2 *
              (pmra_e[i] ** 2 + (pmra[i] * d[i] / d_e[i]) ** 2)),
             ((k * d[i]) ** 2 *
              (pmdec_e[i] ** 2 + (pmdec[i] * d[i] / d_e[i]) ** 2))]
        p = np.array(p)
        q = [B[0, 1] * B[0, 2],
             B[1, 1] * B[1, 2],
             B[2, 1] * B[2, 2]]
        q = np.array(q)
        stdsq = np.dot(np.square(B), p)
        stdsq += (2 * pmra[i] * pmdec[i] * k ** 2 * (1 / d_e[i]) ** 2
                  * (d[i] ** 4) * q)
        assert np.all(stdsq > 0), stdsq
        stdu[i], stdv[i], stdw[i] = np.sqrt(stdsq)

    print('Done!')
    return u, v, w, stdu, stdv, stdw


def compute_dynamics_from_galpy(ra, dec, dist, pmra, pmdec, vlos,
                                v_sun=[-11.1, 220 + 12.24, 7.25],
                                galcen_distance=8.3, Z0=0.027):
    """
    This function only uses functions from galpy to convert astrometric
    observables to Galactocentric velocities and positions.
    This is implemented as a test of the use of astropy functions in
    get_dynamics but can be used instead.

    Parameters
    ----------
    ra : value
        Right ascension in degrees
    dec : value
        Declination in degrees
    pmra : value
        Proper motion in  as/yr
    pmra_e : value
        Uncertainty in proper motion in  as/yr
    pmdec : value
        Proper motion in  as/yr
    pmdec_e : value
        Uncertainty in proper motion in  as/yr
    dist : value
        Distance to the star in parsec
    vlos : value
        Line-of-sight velocity of star in km/s
    v_sun : list
        A list containing the different components of the
        Galactocentric velocity of the Sun.
        Default is the components from Schoenrich et al 2009, with the
        circular velocity of Bovy 2015, v_sun = [-11.1, 220 + 12.24, 7.25].
    galcen_distance : float
        Cylindrical distance to the Galactic Center from the Sun
    Z0 : float
        Height above the Galactic midplane for the Sun

    Returns
    -------
    XYZ_GC : array
        Galactocentric, cartesian coordinates of the star(s)
    Rphiz : array
        Galactocentric, cylindrical coordinates of the star(s)
    vcyl : array
        Galactocentric cylindrical velocities of the star(s)
    """
    if isinstance(ra, float):
        ra = np.array([ra])
        dec = np.array([dec])
        dist = np.array([dist]) / 1e3  # convert to kpc
        pmra = np.array([pmra])
        pmdec = np.array([pmdec])
        vlos = np.array([vlos])
    else:
        dist /= 1e3  # convert to kpc

    # Convert celestrial positions to Galactic coordinates (l, b)
    lb = coords.radec_to_lb(ra, dec, degree=True, epoch=None)

    # Convert to Heliocentric, cartesian coordinates (X_HC, Y_HC, Z_HC)
    XYZ = coords.lbd_to_XYZ(lb[:, 0], lb[:, 1], dist, degree=True)

    # Convert to Galactocentric cylindrical coordinates (R, phi, z)
    Rphiz = coords.XYZ_to_galcencyl(XYZ[:, 0], XYZ[:, 1], XYZ[:, 2],
                                         Xsun=galcen_distance, Zsun=Z0)

    # Convert to Galactocentric, cartesian coordinates (X_GC, Y_GC, Z_GC)
    XYZ_GC = coords.XYZ_to_galcenrect(XYZ[:, 0], XYZ[:, 1], XYZ[:, 2],
                                           Xsun=galcen_distance, Zsun=Z0)

    # Convert proper motions to proper motions in (l, b)
    pmllbb = coords.pmrapmdec_to_pmllpmbb(pmra, pmdec, ra, dec,
                                               degree=True, epoch=None)

    # Convert proper motion in (l, b) and line-of-sight motion to
    # Galactocentric cartesian velocities (v_X, v_Y, v_Z)
    vXYZ = coords.vrpmllpmbb_to_vxvyvz(vlos, pmllbb[:, 0], pmllbb[:, 1],
                                            XYZ[:, 0], XYZ[:, 1], XYZ[:, 2],
                                            XYZ=True, degree=True)

    # Convert Galactocentric cartesian positions and velocites (v_X, v_Y, v_Z)
    # to Galactocentric cylindrical velocites (v_R, v_phi, v_z)
    vcyl = coords.vxvyvz_to_galcencyl(vXYZ[:, 0], vXYZ[:, 1], vXYZ[:, 2],
                                           Rphiz[:, 0], Rphiz[:, 1],
                                           Rphiz[:, 2],
                                           Xsun=galcen_distance, Zsun=Z0,
                                           vsun=v_sun, galcen=True)
    return XYZ_GC, Rphiz, vcyl
