# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# amalie
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###############################################################################
# MODULES
###############################################################################
import sys
from enum import IntFlag

from astropy.table import Table, Column

# Don't print Astropy warnings
import warnings
from astropy.utils.exceptions import AstropyWarning
warnings.filterwarnings('ignore', category=AstropyWarning)

# Import fillvalue from skycats/constants
from skycats import constants


###############################################################################
# Cuts
###############################################################################
def plx_over_err(kitten):
    """
    Cut in uncertainty as Gaia DR2 paper
    """
    print('Make uncertainty cut, length before', len(kitten))
    mask = kitten['PARALLAX_OVER_ERROR_GAIA'] > 5
    kitten = kitten[mask]
    print('Length after', len(kitten))
    return kitten


def ruwe(kitten):
    """
    Cut in RUWE using same cut as in technical note
    """
    print('Make RUWE cut, length before', len(kitten))
    mask = kitten['RUWE_GAIA'] <= 1.4
    kitten = kitten[mask]
    print('Length after', len(kitten))
    return kitten


def excess_factor(kitten):
    """
    In Gaia, astrometric_excess_noise measures the disagreement expressed as
    an angle (mas) between the observations of the source and the best-fitting
    standard astrometric model. A value of 0 signifies that the source is
    astrometrically well-behaved, i.e. that the residuals of the fit
    statistically agree with the assumed observational noise.
    A positive value signifies that the residuals are statistically larger
    than expected.
    This could however also be a sign of a binary star or planets.
    See https://gea.esac.esa.int/archive/documentation/GDR2/Gaia_archive/chap_datamodel/sec_dm_main_tables/ssec_dm_gaia_source.html
    or Lindegren et al 2012
    """
    print('Make cut in excess factor, length before', len(kitten))
    mask = kitten['ASTROMETRIC_EXCESS_NOISE_GAIA'] <= 1.0
    kitten = kitten[mask]
    print('Length after', len(kitten))
    return kitten


def phot_bp_rp_excess_factor(kitten):
    """
    In Gaia DR2, photometric errors mainly in the BP and RP bands, affecting
    in particular faint sources in crowded areas. An indicator of possible
    issues with the BP and RP photometry is the flux excess factor
    E = (IBP + IRP)/IG (phot_bp_rp_excess_factor), where IX is the photometric
    flux in band X (Evans et al. 2018).
    In Lindegren et al. 2018 (The astrometric solution), appendix C the
    following cut in phot_bp_rp_excess_factor is given.
    """
    print('Make cut in phot_bp_rp_excess factor, length before', len(kitten))
    lowmask = (kitten['PHOT_BP_RP_EXCESS_FACTOR_GAIA'] >
               1.0 + 0.015 * (kitten['BP_RP_GAIA']) ** 2)
    highmask = (kitten['PHOT_BP_RP_EXCESS_FACTOR_GAIA'] <
                1.3 + 0.06 * (kitten['BP_RP_GAIA']) ** 2)
    mask = lowmask & highmask
    kitten = kitten[mask]
    print('Length after', len(kitten))
    return kitten


# APOGEEflags
class StarFlag(IntFlag):
    BAD_PIXELS = 1 << 0
    COMMISSIONING = 1 << 2
    VERY_BRIGHT_NEIGHBOR = 1 << 3
    LOW_SNR = 1 << 4
    PERSIST_HIGH = 1 << 9
    PERSIST_MED = 1 << 10
    PERSIST_LOW = 1 << 11
    PERSIST_JUMP_POS = 1 << 12
    PERSIST_JUMP_NEG = 1 << 13
    SUSPECT_RV_COMBINATION = 1 << 16
    SUSPECT_BROAD_LINES = 1 << 17

    badstars = (VERY_BRIGHT_NEIGHBOR |
                LOW_SNR |
                PERSIST_HIGH |
                PERSIST_JUMP_POS |
                PERSIST_JUMP_NEG |
                SUSPECT_RV_COMBINATION
               )

class ASPCAPFlag(IntFlag):
    TEFF_WARN = 1 << 0
    LOGG_WARN = 1 << 1
    VMICRO_WARN = 1 << 2
    METALS_WARN = 1 << 3
    ALPHAFE_WARN = 1 << 4
    CFE_WARN = 1 << 5
    NFE_WARN = 1 << 6
    STAR_WARN = 1 << 7
    CHI2_WARN = 1 << 8
    COLORTE_WARN = 1 << 9
    ROTATION_WARN = 1 << 10
    SN_WARN = 1 << 11
    VSINI_WARN = 1 << 14
    TEFF_BAD = 1 << 16
    LOGG_BAD = 1 << 17
    VMICRO_BAD = 1 << 18
    METALS_BAD = 1 << 19
    ALPHAFE_BAD = 1 << 20
    CFE_BAD = 1 << 21
    NFE_BAD = 1 << 22
    STAR_BAD = 1 << 23
    CHI2_BAD = 1 << 24
    COLORTE_BAD = 1 << 25
    ROTATION_BAD = 1 << 26
    SN_BAD = 1 << 27
    VSINI_BAD = 1 << 30
    NO_ASPCAP_RESULT = 1 << 31

    badstars = (TEFF_BAD |
                LOGG_BAD |
                VMICRO_BAD #|
                #ROTATION_BAD |
                #VSINI_BAD
               )


class ELEMFlag(IntFlag):
    GRIDEDGE_BAD = 1 << 0
    CALRANGE_BAD = 1 << 1
    OTHER_BAD = 1 << 2
    FERRE_FAIL = 1 << 3
    PARAM_MISMATCH_BAD = 1 << 4
    FERRE_ERR_USED = 1 << 5
    TEFF_CUT = 1 << 6
    GRIDEDGE_WARN = 1 << 8
    CALRANGE_WARN = 1 << 9
    OTHER_WARN = 1 << 10
    FERRE_WARN = 1 << 11
    PARAM_MISMATCH_WARN = 1 << 12
    OPTICAL_WARN = 1 << 13
    ERR_WARN = 1 << 14
    FAINT_WARN = 1 << 15
    PARAM_FIXED = 1 << 16
    RV_WARN = 1 << 17
    SPEC_RC = 1 << 24
    SPEC_RGB = 1 << 25
    LOGG_CAL_MS = 1 << 26
    LOGG_CAL_RGB_MS = 1 << 27

    badstars = (GRIDEDGE_BAD |
                CALRANGE_BAD |
                OTHER_BAD |
                FERRE_FAIL #|
                #VSINI_BAD
               )

def get_gestalt(cat, gaiarv=True):
    """
    This function makes the cut for the current definition of gestalt.
    It ensures that the cut is identical in different files.

    Parameters
    ----------
    cat : astropy Table
        The redgiantcat of a given version
    gaiarv : bool, optional
        Flag determining whether or not to use line-of-sight velocities from
        Gaia

    Returns
    -------
    gestaltmask : numpy bool array
        Mask of gestalt stars (True)
    qualitymask : numpy bool array
        Mask of quality cut stars (True)
    chemistrymask : numpy bool array
        Mask of stars with possible spectroscopic values (True)
    kinematicsmask : numpy bool array
        Mask of stars with possible astrometric values (True)
    asteroseismicmask : numpy bool array
        Mask of stars with possible asteroseismic values (True)

    """
    floatfill = funkycat.get_fillvalue(cat['DNU'])

    # Quality cuts
    spectroscopicqualityflags = ((
        (cat['SPECTROSURVEY'] == 'LAMOST') &
        (cat['TEFF_LAMOST'] > 4100) &
        (cat['TEFF_LAMOST'] < 5300) &
        (np.abs(cat['CHI2RATIO_LAMOST']) < 5) &
        (cat['TEFF_FLAG_LAMOST'] != 0) &
        (cat['LOGG_FLAG_LAMOST'] != 0) &
        (cat['VMIC_FLAG_LAMOST'] != 0) &
        (cat['MG_FE_FLAG_LAMOST'] != 0) &
        (cat['SI_FE_FLAG_LAMOST'] != 0) &
        (cat['FE_H_FLAG_LAMOST'] != 0) &
        (cat['FLAG_SINGLESTAR_LAMOST'] == 'YES')
    ) |
    # APOGEE
    ((cat['SPECTROSURVEY'] == 'APOGEE') &
     ((cat['STARFLAG_APOGEE'] & StarFlag.badstars) == 0) &
     ((cat['ASPCAPFLAG_APOGEE'] & ASPCAPFlag.badstars) == 0)
    ) |
    # RAVE
    ((cat['SPECTROSURVEY'] == 'RAVE') &
     (cat['QUALITY_FLAG_RAVE'] == 1)
    ))

    photometric = (cat['QFLG_TMASS'] == 'AAA')

    ruwe = (cat['RUWE_GAIA'] < 1.4)

    quality = spectroscopicqualityflags & photometric & ruwe

    # Chemistry
    chemistry = ((cat['TEFF'] != floatfill) &
                 (cat['FE_H'] != floatfill) &
                 (cat['ALPHA_FE'] != floatfill))

    # Kinematics
    if gaiarv:
        kinematics = ((cat['PMRA_GAIA'] != floatfill) &
                      (cat['PMDEC_GAIA'] != floatfill) &
                      (cat['RADIAL_VELOCITY_GAIA'] != floatfill) &
                      (cat['PARALLAX_GAIA'] != floatfill))
    else:
        kinematics = ((cat['PMRA_GAIA'] != floatfill) &
                      (cat['PMDEC_GAIA'] != floatfill) &
                      (cat['PARALLAX_GAIA'] != floatfill))

    # Asteroseismic
    asteroseismic = ((cat['DNU'] != floatfill) &
                     (cat['NUMAX'] != floatfill))

    gestalt = quality & chemistry & kinematics & asteroseismic
    print('Lenght of original cat is', len(cat))
    print('Lenght of gestalt kitten is', np.sum(gestalt))
    return gestalt, quality, chemistry, kinematics, asteroseismic
