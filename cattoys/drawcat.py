# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# amalie
# Scipts to make consistent plots for cattoys/skycats
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###############################################################################
# MODULES
###############################################################################
import os
import re
import sys
import numpy as np

import astropy.units as u
from astropy.table import Table
import astropy.coordinates as coord

from natsort import natsorted
import pandas as pd
import statsmodels.api as sm
from scipy.optimize import curve_fit

import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import seaborn as sns

from mw_plot import MWPlot, MWSkyMap

from cattoys import toolcat as tool
from skycats import funkycat

# Don't print Astropy warnings
import warnings
from astropy.utils.exceptions import AstropyWarning
warnings.filterwarnings('ignore', category=AstropyWarning)

# Make some plot settings
sns.set_palette('colorblind')
sns.set_color_codes('colorblind')
sns.set_style({"xtick.direction": "inout", "ytick.direction": "inout",
               "xtick.top": True, "ytick.right": True})

color = {'COROGEE': '#222255',
         'K2C1': '#332288', 'K2C2': '#0077BB', 'K2C3': '#33BBEE',
         'K2C4': '#225522', 'K2C5': '#117733', 'K2C6': '#009988',
         'K2C7': '#44BB99', 'K2C8': '#666633', 'K2C9': '#000000',
         'K2C10': '#999933', 'K2C11': '#CCBB44', 'K2C12': '#EEDD88',
         'K2C13': '#663333', 'K2C14': '#AA4499', 'K2C15': '#EE3377',
         'K2C16': '#FFAABB', 'K2C17': '#882255', 'K2C18': '#CC3311',
         'Kepler': '#EE7733',
         'K2C19': '#FFFFFF', 'K2C20': '#CC6677', 'K2C21': '#CCCCCC',
        }


def filter_points(xs, ys, delta=0.2):
    xs = xs.value
    ys = ys.value
    xmin = xs.min()
    ymin = ys.min()
    xs_grid = np.round((xs - xmin) / delta).astype(np.intp)
    ys_grid = np.round((ys - ymin) / delta).astype(np.intp)
    xlen = xs_grid.max() + 1
    ylen = ys_grid.max() + 1
    which = -np.ones(shape=(ylen, xlen), dtype=np.intp)
    which[ys_grid, xs_grid] = np.arange(len(xs_grid))
    indices = which.ravel()
    indices = indices[indices != -1]
    return indices


###############################################################################
# Normal plots
###############################################################################
def scatter(plotsname, idstrs, data, xerr=None, yerr=None,
            units=None, labelcol=None,
            xscale='linear', yscale='linear',
            xinverse=False, yinverse=False, hist2d=False,
            xlim=None, ylim=None, masks=None, masksidstrs=None, note=None,
            compare=False):
    """
    This generic function makes a scatter plot for two given columns.

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    idstrs : list of str
        List containing identifying names of columns plotted.
    data : list of columns
        data[1] plotted as a function of data[0].
        If len(data) == 3, points are colorcoded by data[2].
    xerr : list, optional
        Errors on x-values, can be left None.
    yerr : list, optional
        Errors on y-values, can be left None.
    units : list of str, optional
        Unit of data added to plot.
        Default is None, and if unit is None, no units are added to the plot.
    labelcol : column, optional
        the LABEL column of the catalogue
    xscale : str, optional
        The scale on the x-axis. Default is 'linear'.
    yscale : str, optional
        The scale on the y-axis. Default is 'linear'.
    xinverse : bool, optional
        Flag determining whether the x-axis should be inverted.
        Default is False.
    yinverse : bool, optional
        Flag determining whether the y-axis should be inverted.
        Default is False.
    hist2d : bool, optional
        Flag determining if scatter plot should be 2D histogram.
        This only has an effect if len(data) == 2 and if labelbol is None.
        Default is False.
    xlim : list, optional
        The limits of the x-axis. Default is None
    ylim : list, optional
        The limits of the y-axis. Default is None
    masks : list or None, optional
        List of possible masks to apply to the data.
        The masks will be applied one by one. The ordering needs to be the
        same as masksidstrs.
        Default is None.
    masksidstrs : list or None, optional
        List of idstrs of the possible masks applied to the data.
        These istrs are used in the legend.
        Default is None.
    note : str, optional
        String used in name.
    compare : flag, optional
        If True, add line at x=y
    """
    assert len(idstrs) >= 2 & len(idstrs) < 4
    if (len(idstrs) == 3) & (labelcol is None):
        print('Plot scatter plot of %s vs %s colorcoded by %s' %
              (idstrs[0], idstrs[1], idstrs[2]))
    else:
        print('Plot scatter plot of %s vs %s' % (idstrs[0], idstrs[1]))
    assert isinstance(plotsname, str)
    assert len(idstrs) == len(data)
    if units is not None:
        assert len(units) == len(data)

    if (len(idstrs) == 3) & (labelcol is None):
        name = ('_scatter_'
                + idstrs[0] + '_' + idstrs[1] + '_cc_' + idstrs[2])
    else:
        name = ('_scatter_'
                + idstrs[0] + '_' + idstrs[1])
    if units is not None:
        name += '_u'
    if hist2d:
        name += '_hist2d'
    if labelcol is not None:
        name += '_labelled'
    if masks is not None:
        name += '_masked'
    if note is not None:
        name += '_' + note
    name = tool.remove_dumbchars(name)
    name = name + '.pdf'
    name = plotsname + name

    pp = PdfPages(name)

    fillmask = np.ones(len(data[0]), dtype=bool)
    for i, d in enumerate(data):
        fillvalue = funkycat.get_fillvalue(d)
        fillmask &= (d != fillvalue)
        if xerr is not None:
            fillmask &= (xerr[i] != fillvalue)
        if xerr is not None:
            fillmask &= (yerr[i] != fillvalue)

    print('Points in common: %s out of %s' % (np.sum(fillmask),
                                              len(fillmask)))

    fig, ax = plt.subplots()
    if labelcol is not None:
        labels = tool.get_labels(labelcol)
        for label in labels:
            mask = (labelcol == label)
            mask = mask & fillmask
            print('In %s vs %s, label %s: %s stars' % (idstrs[0], idstrs[1],
                                                       label, np.sum(mask)))
            if xerr is not None or yerr is not None:
                if len(xerr) > 1:
                    if len(xerr) == 2:
                        maskxerr = [xerr[0][mask], xerr[1][mask]]
                    else:
                        maskxerr = xerr[mask]
                else:
                    maskxerr = xerr
                if len(yerr) > 1:
                    if len(yerr) == 2:
                        maskyerr = [yerr[0][mask], yerr[1][mask]]
                    else:
                        maskyerr = yerr[mask]
                else:
                    maskyerr = yerr
                ax.errorbar(data[0][mask], data[1][mask],
                            xerr=maskxerr, yerr=maskyerr,
                            fmt='none', alpha=0.3,
                            linestyle=None, ecolor='0.9',
                            zorder=-1, label=None)
            points = ax.scatter(data[0][mask], data[1][mask],
                                s=1, alpha=0.8,
                                label=label, c=color[label])
        ax.legend(borderaxespad=0., fontsize='small',
                  frameon=False, markerscale=6)
    elif (len(idstrs) == 3) & (labelcol is None):
        mask = fillmask
        if xerr is not None or yerr is not None:
            if len(xerr) > 1:
                if len(xerr) == 2:
                    maskxerr = [xerr[0][mask], xerr[1][mask]]
                else:
                    maskxerr = xerr[mask]
            else:
                maskxerr = xerr
            if len(yerr) > 1:
                if len(yerr) == 2:
                    maskyerr = [yerr[0][mask], yerr[1][mask]]
                else:
                    maskyerr = yerr[mask]
            else:
                maskyerr = yerr
            ax.errorbar(data[0][mask], data[1][mask],
                        xerr=maskxerr, yerr=maskyerr,
                        fmt='none', alpha=0.3,
                        ecolor='0.9',
                        linestyle=None, zorder=-1, label=None)
        points = ax.scatter(data[0][mask], data[1][mask],
                            s=1, alpha=0.8,
                            c=data[2][mask], cmap='viridis_r')
        cb = tool.colorbar(points)
        if units is not None:
            if units[2] is not None:
                cb.ax.set_ylabel('%s (%s)' % (idstrs[2], units[2]))
            else:
                cb.ax.set_ylabel('%s' % idstrs[2])
        else:
            cb.ax.set_ylabel('%s' % idstrs[2])
    elif masks is not None:
        assert masksidstrs is not None
        for mask, maskidstr in zip(masks, masksidstrs):
            mask &= fillmask
            print('In %s vs %s, label %s: %s stars' % (idstrs[0], idstrs[1],
                                                       maskidstr, np.sum(mask)))
            if xerr is not None or yerr is not None:
                if len(xerr) > 1:
                    if len(xerr) == 2:
                        maskxerr = [xerr[0][mask], xerr[1][mask]]
                    else:
                        maskxerr = xerr[mask]
                else:
                    maskxerr = xerr
                if len(yerr) > 1:
                    if len(yerr) == 2:
                        maskyerr = [yerr[0][mask], yerr[1][mask]]
                    else:
                        maskyerr = yerr[mask]
                else:
                    maskyerr = yerr
                ax.errorbar(data[0][mask], data[1][mask],
                            xerr=maskxerr, yerr=maskyerr,
                            fmt='none', alpha=0.3, ecolor='0.9',
                            linestyle=None, zorder=-1, label=None)
            ax.scatter(data[0][mask], data[1][mask],
                       s=1, alpha=0.8, label=maskidstr)
        ax.legend(borderaxespad=0., fontsize='small',
                  frameon=False, markerscale=6)
    else:
        mask = fillmask
        if hist2d:
            h = ax.hist2d(data[0][mask], data[1][mask],
                          bins=80, cmap=plt.cm.magma, cmin=1,
                          norm=mcolors.LogNorm())
            plt.colorbar(h[3], ax=ax)
        else:
            if xerr is not None or yerr is not None:
                if len(xerr) > 1:
                    if len(xerr) == 2:
                        maskxerr = [xerr[0][mask], xerr[1][mask]]
                    else:
                        maskxerr = xerr[mask]
                else:
                    maskxerr = xerr
                if len(yerr) > 1:
                    if len(yerr) == 2:
                        maskyerr = [yerr[0][mask], yerr[1][mask]]
                    else:
                        maskyerr = yerr[mask]
                else:
                    maskyerr = yerr
                ax.errorbar(data[0][mask], data[1][mask],
                            xerr=maskxerr, yerr=maskyerr,
                            fmt='none', alpha=0.3,
                            linestyle=None, ecolor='0.9',
                           zorder=-1)
            points = ax.scatter(data[0][mask], data[1][mask],
                                s=1, alpha=0.5, c='b')

    if xinverse:
        ax.invert_xaxis()
    if yinverse:
        ax.invert_yaxis()
    if units is None:
        ax.set_xlabel('%s' % idstrs[0])
        ax.set_ylabel('%s' % idstrs[1])
    else:
        if units[0] is not None:
            ax.set_xlabel('%s (%s)' % (idstrs[0], units[0]))
        else:
            ax.set_xlabel('%s' % idstrs[0])
        if units[1] is not None:
            ax.set_ylabel('%s (%s)' % (idstrs[1], units[1]))
        else:
            ax.set_ylabel('%s' % idstrs[1])
    if xlim is not None:
        ax.set_xlim(xlim[0], xlim[1])
    if ylim is not None:
        ax.set_ylim(ylim[0], ylim[1])

    if compare:
        lims = [
            np.min([ax.get_xlim(), ax.get_ylim()]),  # min of both axes
            np.max([ax.get_xlim(), ax.get_ylim()]),  # max of both axes
        ]
        ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)

    ax.set_xscale(xscale)
    ax.set_yscale(yscale)
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def hist(plotsname, idstr, x, unit=None, xlim=None, ylim=None):
    """
    This generic function makes a histogram of a given column.

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    idstr : str
        Identifying name of specific histogram
    x : column
        Data plotted in histogram
    unit : str, optional
        Unit of x added to plot.
        Default is None, and if unit is None, no unit is added to the plot.
    kde_kws : None or dict, optional
        Keyword arguments for kdeplot().
    """
    print('Plot histogram of', idstr)
    assert isinstance(plotsname, str)
    if isinstance(idstr, str):
        name = '_kde_' + idstr
    else:
        name = '_kde'
        for i in idstr:
            name += '_' + i
    name = tool.remove_dumbchars(name)
    if unit is not None:
        name += '_u'
    name = name + '.pdf'
    name = plotsname + name

    pp = PdfPages(name)
    plt.figure()

    if isinstance(x, list):
        xlabel = ''
        for i, col in enumerate(x):
            fillvalue = funkycat.get_fillvalue(col)
            mask = (col != fillvalue)
            print('In %s: %s out of %s' % (idstr[i], np.sum(mask), len(col)))
            sns.histplot(col.astype(float)[col != fillvalue],
                         kde=True, label=idstr[i])
            print('In %s, %s data points' % (idstr[i],
                                             len(col[col != fillvalue])))
            xlabel += idstr[i] + ' vs. '
        plt.legend()
        plt.xlabel(xlabel)
    elif x.dtype.kind in ['S', 'U', 'b']:
        fillvalue = funkycat.get_fillvalue(x)
        mask = (x != fillvalue)
        print('In %s: %s out of %s' % (idstr, np.sum(mask), len(x)))
        sns.countplot(x=natsorted(x[x != fillvalue]))
        plt.xticks(rotation=50)
    else:
        fillvalue = funkycat.get_fillvalue(x)
        print('Points: %s out of %s' % (np.sum(x != fillvalue), len(x)))
        sns.histplot(x[x != fillvalue],
                    kde=True, label=idstr)
        print('In %s, %s data points' % (idstr, len(x[x != fillvalue])))

        if unit is None:
            plt.xlabel('%s' % idstr)
        else:
            plt.xlabel('%s (%s)' % (idstr, unit))
    if xlim is not None:
        plt.xlim(xlim[0], xlim[1])
    if ylim is not None:
        plt.ylim(ylim[0], ylim[1])
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def corner(plotsname, idstrs, cols, units=None, labelcol=None):
    """
    This generic function makes a corner plot for any number of given columns.

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    idstrs : list of str
        List containing identifying names of columns plotted.
    cols : list of columns
        List containing all columns to be plotted in the corner plot.
    units : list of str, optional
        Unit of data added to plot.
        Default is None, and if unit is None, no units are added to the plot.
    labelcol : column, optional
        the LABEL column of the catalogue
    """
    print('Plot corner plot of', idstrs)
    assert isinstance(plotsname, str)
    assert len(idstrs) == len(cols)

    name = '_corner'
    for idstr in idstrs:
        name += '_' + idstr
    name = tool.remove_dumbchars(name)
    if units is not None:
        name += '_u'
    if labelcol is not None:
        name += '_labelled'
    name += '.png'
    name = plotsname + name

    df = tool.table_to_df(cols, idstrs, units=units, labelcol=labelcol)

    if labelcol is None:
        g = sns.PairGrid(df)
    else:
        g = sns.PairGrid(df, hue='Survey')
    g = g.map_upper(plt.scatter, edgecolor='w', alpha=0.5)
    g = g.map_lower(sns.kdeplot, cmap='Blues_d')
    g = g.map_diag(sns.histplot, kde=True)
    if labelcol is not None:
        g = g.add_legend()

    g.savefig(name)
    plt.close()
    print('Done!')


###############################################################################
# Plots of positions, velocities, and actions
###############################################################################
def galacticsky(plotsname, ls, bs, labelcol=None):
    """
    Plot with image of galaxy in the background in (l, b).
    """
    name = plotsname + '_galsky.pdf'
    pp = PdfPages(name)

    fillmask = (ls != funkycat.get_fillvalue(ls))
    ls = coord.Angle(ls.data * u.degree)
    bs = coord.Angle(bs.data * u.degree)

    here = os.path.realpath(os.path.dirname(__file__))
    img = plt.imread(os.path.join(here, 'gaiadensity.jpg'))

    fig = plt.figure(figsize=(15,15))
    ax2 = fig.add_subplot(111, projection='hammer')
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.gca().axes.get_yaxis().set_visible(False)
    # As this projection does not allow for invert the axis,
    # we flip the sign of x-axis instead
    # plt.gca().invert_xaxis()

    if labelcol is None:
        ax2.scatter(-ls.radian[fillmask], bs.radian[fillmask],
                    s=1, c='orange')
        ax2.scatter(-(ls.radian[fillmask]-2*np.pi), bs.radian[fillmask],
                    s=1, c='orange')
    else:
        labels = tool.get_labels(labelcol)
        for label in labels:
            mask = (labelcol == label)
            mask = mask & fillmask
            ax2.scatter(-ls.radian[mask], bs.radian[mask],
                        s=1, c=color[label], label=label) #'orange')
            ax2.scatter(-(ls.radian[mask]-2*np.pi), bs.radian[mask],
                        s=1, c=color[label]) #'orange')
            ax2.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=8, ncol=5,
                       mode='expand', markerscale=6, fontsize='x-small')

    ax2.patch.set_alpha(0.0)
    x=-0.037
    y=-0.17
    extent = [x, 1-x, y, 1-y]
    ax2.imshow(
        img,
        zorder=0,
        cmap="gray",
        rasterized=True,
        aspect=ax2.get_aspect(),
        transform=ax2.transAxes,
        extent=extent,
    )
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def celestialsky(plotsname, ra, dec, labelcol=None,
                  ra_mark=None, dec_mark=None, anot=None, figsize=[8.47,5.23]):
    """
    This makes a plot of the given celestial positions in a Mollweide
    projection. This is mostly a figure for introductory purposes.

    Parameters
    ----------
    plotsname : str
        Identifying name for this sample of stars.
    ra : astropy Column
        Right ascension of the sample in degrees.
    dec : astropy Column
        Declination of the sample in degrees.
    labelcol : astropy Column, optional
        the LABEL column of the catalogue.
        If left None, no labels will be used.
    ra_mark : array, optional
        Array of right ascensions of stars that we want to highlight.
        Default is `None` and in that case no stars will be highlighted.
    dec_mark : array, optional
        Array of declinations of stars that we want to highlight.
        Default is `None` and in that case no stars will be highlighted.
    anot : array, optional
        Array of annotations for the highligted stars.
        Default is `None` and in that case no stars will be highlighted.
    """
    name = plotsname + '_skychart.pdf'
    pp = PdfPages(name)

    floatfill = funkycat.get_fillvalue(ra)
    fillmask = ((ra != floatfill) & (dec != floatfill))
    ra = coord.Angle(ra.data * u.degree)
    dec = coord.Angle(dec.data * u.degree)

    if ra_mark is not None:
        ra_mark = coord.Angle(ra_mark * u.degree)
        dec_mark = coord.Angle(dec_mark * u.degree)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, projection="mollweide")
    if labelcol is not None:
        labels = tool.get_labels(labelcol)
        for label in labels:
            mask = (labelcol == label)
            mask = mask & fillmask
            ax.scatter(ra.radian[mask], dec.radian[mask], s=2,
                       c=color[label], label=label)
            ax.scatter(ra.radian[mask]-2*np.pi, dec.radian[mask], s=2,
                       c=color[label])
        ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=8, ncol=5,
                  mode='expand', markerscale=6, fontsize='medium')
    else:
        mask = fillmask
        ax.scatter(ra.radian[mask]-np.pi, dec.radian[mask],
                   s=2, rasterized=True)

    if ra_mark is not None:
        ax.scatter(ra_mark.radian-np.pi, dec_mark.radian,
                   s=8, c='r', marker='*')
        for i, txt in enumerate(anot):
            ax.annotate(txt, (ra_mark.radian[i], dec_mark.radian[i]),
                        size='x-small')
            ax.annotate(txt, (ra_mark.radian[i]-2*np.pi, dec_mark.radian[i]),
                        size='x-small')
    ax.tick_params(axis='both', which='major', labelsize=12)
    ax.set_xticklabels([r'30$\degree$', r'60$\degree$', r'90$\degree$',
                        r'120$\degree$', r'150$\degree$', r'180$\degree$',
                        r'210$\degree$', r'240$\degree$', r'270$\degree$',
                        r'300$\degree$', r'330$\degree$'], color='0.75')
    ax.grid(True)
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def galacticpositions(plotsname, R, z, labelcol=None,
                      add_TESS=False, add_gaia=True, simpleplot=False, pms=None, png=True, loc=1):
    """
    This makes a plot of the positions of the stars in a Galactocentric frame

    Parameters
    ----------
    plotsname : str
        Identifying name for this sample of stars.
    R : astropy Column
        Galactocentric cylindrical distance R
    z : astropy Column
        Galactocentric cylindrical height z
    labelcol : astropy Column, optional
        the LABEL column of the catalogue.
        If left None, no labels will be used.
    add_TESS : bool, optional
        Flag to add an estimate of the TESS sphere of observations
        Default is True
    pms : list, optional
        List containing the columns of proper motion in right ascension ([0])
        and proper motion in declination ([1]). If this list is given, the
        scatter points will be rescaled by the size of the proper motion.
        The larger point, the higher proper motion.
        If None, no rescaling will be performed.
    png : bool, optional
        Flag to determine whether to output the plot as png. Default is False.
    """
    print('Make plot of galactic positions')
    assert isinstance(plotsname, str)
    name = '_galacticpositions'
    if add_TESS:
        if png:
            name += '_tess.png'
        else:
            name += '_tess.pdf'
    elif add_gaia:
        if png:
            name += '_gaia.png'
        else:
            name += '_gaia.pdf'
    else:
        if png:
            name += '.png'
        else:
            name += '.pdf'
    name = plotsname + name

    floatfill = funkycat.get_fillvalue(R)
    fillmask = (R != floatfill) & (z != floatfill)

    # Rescale the points as a function of proper motion
    if pms is not None:
        pm_ra_cosdec = pms[0].data
        pm_dec = pms[1].data
        s = np.sqrt(pm_ra_cosdec ** 2 + pm_dec ** 2)
    else:
        s = np.ones(len(R.data))

    if not png:
        pp = PdfPages(name)
    plt.figure()
    #plt.axis('equal')
    if add_TESS:
        circle = plt.Circle((8.34, 0), radius=3, linestyle='--',
                            edgecolor='0.5', fill=False, label='TESS')
        plt.gca().add_patch(circle)
    elif add_gaia:
        circle = plt.Circle((8.34, 0), radius=9.1, linestyle='--',
                            edgecolor='0.5', fill=False, label='Gaia')
        plt.gca().add_patch(circle)
    if labelcol is not None:
        labels = tool.get_labels(labelcol)
        for label in labels:
            mask = (labelcol == label)
            mask = mask & fillmask
            if label == 'Kepler':
                zorder = 0
            else:
                zorder = 1
            if simpleplot:
                if 'K2' in label:
                    plt.scatter(R[mask], z[mask], s=s[mask], alpha=0.5,
                                zorder=zorder, c=color[label], marker='o')
                elif label == 'Kepler':
                    plt.scatter(R[mask], z[mask], s=s[mask], alpha=0.5,
                                zorder=zorder, c=color[label], marker='*')
                elif label == 'COROGEE':
                    plt.scatter(R[mask], z[mask], s=s[mask], alpha=0.5,
                                zorder=zorder, c=color[label], marker='s')
            else:
                plt.scatter(R[mask], z[mask], s=s[mask], alpha=0.5,
                            zorder=zorder, c=color[label], label=label)
        if simpleplot:
            plt.scatter(R[0], z[0], s=s[0], zorder=-1, c='k', marker='o', label='K2')
            plt.scatter(R[0], z[0], s=s[0], zorder=-1, c='k', marker='*', label='Kepler')
            plt.scatter(R[0], z[0], s=s[0], zorder=-1, c='k', marker='s', label='CoRoT')
            lgnd = plt.legend(loc=loc, frameon=False, scatterpoints=1,
                              mode='expand', markerscale=6, fontsize='x-small')
        else:
            lgnd = plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=8, ncol=5,
                              frameon=False, scatterpoints=1,
                              mode='expand', markerscale=6, fontsize='x-small')
        for handle in lgnd.legendHandles:
            handle._sizes = ([10])
    else:
        mask = fillmask
        plt.scatter(R[mask], z[mask], s=s[mask], alpha=0.5)

    plt.xlabel(r'R$_{\mathrm{GC}}$ (kpc)')
    plt.ylabel('Z (kpc)')
    plt.xlim([-0.25, 15.75])
    plt.ylim([-5.75, 7.01])
    if png:
        plt.savefig(name, bbox_inches='tight')
        plt.close()
    else:
        pp.savefig(bbox_inches='tight')
        pp.close()
    print('Done!')


def quiver(plotsname, ra, dec, pmra, pmdec, parallax):
    """
    Make quiver plot of observed astrometric parameters
    Works best for areas less than 1 deg x 1 deg.

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    ra : astropy Column
        Right ascension of the sample in degrees.
    dec : astropy Column
        Declination of the sample in degrees.
    pmra : astropy Column
        Proper motion in right ascension of the sample in mas/yr
    pmdec : astropy Column
        Proper motion in declination of the sample in mas/yr
    parallax : astropy Column
        Parallax of the sample in mas
    """
    print('Plot quiver plot')
    assert isinstance(plotsname, str)
    name = plotsname + '_quiver.pdf'
    pp = PdfPages(name)

    cols = [ra, dec, pmra, pmdec, parallax]
    idstrs = ['ra', 'dec', 'pmra', 'pmdec', 'parallax']
    df = tool.table_to_df(cols, idstrs)

    plt.figure()
    plt.quiver(df['ra'], df['dec'], df['pmra'], df['pmdec'], df['parallax'])
    plt.colorbar(label='parallax (mas)')
    plt.ylabel('dec (deg)')
    plt.xlabel('ra (deg)')
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def compare_coordinates(plotsname, ra1, dec1, idstr1, ra2, dec2, idstr2):
    print('Plot a comparison between coordinates')
    assert isinstance(plotsname, str)
    name = plotsname + '_comparison_coords_' + idstr1 + '_' + idstr2 + '.pdf'
    fillvalue = funkycat.get_fillvalue(ra1)
    mask = (ra1 != fillvalue) & (ra2 != fillvalue)

    coord1 = coord.SkyCoord(ra=ra1[mask].data * u.deg,
                            dec=dec1[mask].data * u.deg)
    coord2 = coord.SkyCoord(ra=ra2[mask].data * u.deg,
                            dec=dec2[mask].data * u.deg)

    diff = coord1.separation(coord2)

    pp = PdfPages(name)
    plt.figure()
    y, x, _ = plt.hist(diff.value, density=False, bins='fd')
    plt.axvline(x=0, linestyle='--', c='0.6')
    elem = np.argmax(y)
    plt.axvline(x=x[elem], linestyle=':', c='0.6')
    plt.xlabel(r'Angular separation between ' + idstr1 +
               ' and ' + idstr2 + '(deg)')
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def toomre(plotsname, u, v, w, err_u, err_v, err_w,
           label1=None, label2=None,
           u2=None, v2=None, w2=None,
           err_u2=None, err_v2=None, err_w2=None,
           kde=True, hexi=True, logcolor=True):
    """
    This plots a Toomre diagram, a plot which shows the distribution in
    Galactic velocities components in the heliocentric frame.
    Stars that move like the Sun are thus found near origo in this plot.

    Parameters
    ----------
    u : astropy Column
        Heliocentric velocity in radial direction in km/s
    v : astropy Column
        Heliocentric velocity in azimuthal direction in km/s
    w : astropy Column
        Heliocentric velocity in vertical direction in km/s
    err_u : astropy Column
        Uncertainty in heliocentric velocity in radial direction in km/s
    err_v : astropy Column
        Uncertainty in heliocentric velocity in azimuthal direction in km/s
    err_w : astropy Column
        Uncertainty in heliocentric velocity in vertical direction in km/s
    """
    print('Plot Toomre diagram')
    name = plotsname + '_toomre.pdf'
    pp = PdfPages(name)

    fig, ax = plt.subplots()
    plt.axis('equal')
    std = (np.sqrt(err_u ** 2 + err_v ** 2 + err_w ** 2)).tolist()
    uw = np.sqrt(u ** 2 + w ** 2)
    for radius in [100, 200, 300, 400, 500, 600, 700]:
        circle = plt.Circle((0, 0), radius=radius,
                            linestyle='--', edgecolor='0.5', fill=False)
        ax.add_artist(circle)
    if logcolor:
        points = ax.scatter(v, uw, s=1, alpha=0.4, rasterized=True,
                            c=np.log(std), cmap='viridis')
        cb = tool.colorbar(points)
        cb.ax.set_ylabel(r'log($\sigma_U^2 + \sigma_V^2 + \sigma_W^2$) (km/s)')
    else:
        points = ax.scatter(v, uw, s=1, alpha=0.4,
                            rasterized=True, c=std, cmap='viridis')
        cb = tool.colorbar(points)
        cb.ax.set_ylabel(r'$\sigma_U^2 + \sigma_V^2 + \sigma_W^2$ (km/s)')
    ax.set_xlabel(r'V (km/s)')
    ax.set_ylabel(r'(U$^2$ + W$^2$)$^{1/2}$ (km/s)')
    ax.set_ylim([0, 650])
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')

    if kde:
        print('Plot Toomre kde diagram')
        name = plotsname + '_toomre_kde.pdf'
        pp = PdfPages(name)
        fig, ax = plt.subplots()
        for radius in [100, 200, 300, 400, 500, 600, 700]:
            circle = plt.Circle((0, 0), radius=radius,
                                linestyle='--', edgecolor='0.5', fill=False)
            ax.add_artist(circle)
        sns.kdeplot(v, uw, shade=True, gridsize=110)
        ax.set_xlim([-390, 110])
        ax.set_ylim([0, 385])
        ax.set_xlabel(r'V (km/s)')
        ax.set_ylabel(r'(U$^2$ + W$^2$)$^{1/2}$ (km/s)')
        pp.savefig(bbox_inches='tight')
        pp.close()
        print('Done!')

    if hexi:
        print('Plot Toomre hex plot')
        name = plotsname + '_toomre_hex.pdf'
        pp = PdfPages(name)
        plt.axis('equal')
        fig, ax = plt.subplots()
        for radius in [100, 200, 300, 400, 500, 600, 700]:
            circle = plt.Circle((0, 0), radius=radius,
                                linestyle='--', edgecolor='0.5', fill=False)
            ax.add_artist(circle)
        ax.hexbin(v, uw, gridsize=200, cmap='inferno', mincnt=1)
        ax.axis([-390, 110, 0, 385])
        ax.set_xlabel(r'V (km/s)')
        ax.set_ylabel(r'(U$^2$ + W$^2$)$^{1/2}$ (km/s)')
        pp.savefig(bbox_inches='tight')
        pp.close()
        print('Done!')

    if u2 is not None:
        print('Plot Toomre comparison')
        name = plotsname + '_toomre_compare.pdf'
        uw2 = np.sqrt(u2 ** 2 + w2 ** 2)
        pp = PdfPages(name)
        plt.axis('equal')
        fig, ax = plt.subplots()
        for radius in [100, 200, 300, 400, 500, 600, 700]:
            circle = plt.Circle((0, 0), radius=radius,
                                linestyle='--', edgecolor='0.5', fill=False)
            ax.add_artist(circle)
        ax.scatter(v, uw, s=1, alpha=0.4, color='b',
                   rasterized=True, label=label1)
        ax.scatter(v2, uw2, s=1, alpha=0.4, color='m',
                   rasterized=True, label=label2)
        ax.set_xlabel(r'V (km/s)')
        ax.set_ylabel(r'(U$^2$ + W$^2$)$^{1/2}$ (km/s)')
        ax.legend(borderaxespad=0., fontsize='small', frameon=False,
                  markerscale=6)
        ax.set_ylim([0, 650])
        pp.savefig(bbox_inches='tight')
        pp.close()
        print('Done!')


def toomre_gc(plotsname, vr, vphi, vz, err_vr, err_vphi, err_vz, r,
              kde=True, hexi=True, logcolor=True):
    """
    This plots a Toomre diagram in GC, a plot which shows the distribution in
    Galactic velocities components in the galactocentric frame.
    Stars that move like the Sun are thus found near 220 km/s in this plot.

    Parameters
    ----------
    vr : astropy Column
        Galactocentric velocity in radial direction in km/s
    vphi : astropy Column
        Galactocentric velocity in azimuthal direction in km/s
    vz : astropy Column
        Galactocentric velocity in vertical direction in km/s
    err_u : astropy Column
        Uncertainty in Galactocentric velocity in radial direction in km/s
    err_v : astropy Column
        Uncertainty in Galactocentric velocity in azimuthal direction in km/s
    err_w : astropy Column
        Uncertainty in Galactocentric velocity in vertical direction in km/s
    """
    print('Plot Toomre diagram in GC')
    name = plotsname + '_toomre_gc.pdf'
    pp = PdfPages(name)

    vt = vphi * r/8

    fig, ax = plt.subplots()
    plt.axis('equal')
    std = (np.sqrt(err_vr ** 2 + err_vphi ** 2 + err_vz ** 2)).tolist()
    vrvz = np.sqrt(vr ** 2 + vz ** 2)
    for radius in [100, 200, 300, 400, 500, 600, 700]:
        circle = plt.Circle((220, 0), radius=radius,
                            linestyle='--', edgecolor='0.5', fill=False)
        ax.add_artist(circle)
    if logcolor:
        points = ax.scatter(vt, vrvz, s=1, alpha=0.4, rasterized=True,
                            c=np.log(std), cmap='viridis')
        cb = tool.colorbar(points)
        cb.ax.set_ylabel(r'log($\sigma_U^2 + \sigma_V^2 + \sigma_W^2$) (km/s)')
    else:
        points = ax.scatter(vt, vrvz, s=1, alpha=0.4,
                            rasterized=True, c=std, cmap='viridis')
        cb = tool.colorbar(points)
        cb.ax.set_ylabel(r'$\sigma_R^2 + \sigma_{$\phi$}^2 + \sigma_z^2$ (km/s)')
    ax.set_xlabel(r'$V_T$ (km/s)')
    ax.set_ylabel(r'($V_R^2$ + $V_z^2$)$^{1/2}$ (km/s)')
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')

    if kde:
        print('Plot Toomre kde diagram')
        name = plotsname + '_toomre_kde.pdf'
        pp = PdfPages(name)
        fig, ax = plt.subplots()
        for radius in [100, 200, 300, 400, 500, 600, 700]:
            circle = plt.Circle((220, 0), radius=radius,
                                linestyle='--', edgecolor='0.5', fill=False)
            ax.add_artist(circle)
        sns.kdeplot(vt, vrvz, shade=True, gridsize=110)
        ax.set_xlabel(r'$V_T$ (km/s)')
        ax.set_ylabel(r'($V_R^2$ + $V_z^2$)$^{1/2}$ (km/s)')
        pp.savefig(bbox_inches='tight')
        pp.close()
        print('Done!')

    if hexi:
        print('Plot Toomre hex plot')
        name = plotsname + '_toomre_hex.pdf'
        pp = PdfPages(name)
        plt.axis('equal')
        fig, ax = plt.subplots()
        for radius in [100, 200, 300, 400, 500, 600, 700]:
            circle = plt.Circle((220, 0), radius=radius,
                                linestyle='--', edgecolor='0.5', fill=False)
            ax.add_artist(circle)
        ax.hexbin(vt, vrvz, gridsize=200, cmap='inferno', mincnt=1)
        ax.set_xlabel(r'$V_T$ (km/s)')
        ax.set_ylabel(r'($V_R^2$ + $V_z^2$)$^{1/2}$ (km/s)')
        pp.savefig(bbox_inches='tight')
        pp.close()
        print('Done!')


def mwplot(plotsname, x, y, mode='face-on', coord='galactocentric',
           radius=8, z=None, clim=None):
    """
    Use MWPlot to make a plot with the Galaxy in the background

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    x : astropy Column
        x coordinate. Physical meaning of x depend on `coord` and `mode`.
    y : astropy Column
        y coordinate. Physical meaning of y depend on `coord` and `mode`.
    mode : str
        The view of the Galaxy plotted. Can be 'face-on' or 'edge-on'.
    coord : str
        The type of coordinates plotted. Can be 'galactocentric' or 'galactic'.
    radius : float
        Radius of the plot in kpc.
    z : None or list with astropy Column and str, optional
        Column to colorcode by along with colorbar title [z, 'colorbar_title'].
        If None no colorcoding is used.  Default is None
    clim : tuble or None, optional
        Colorbar range
    """
    print('Make plot of galactic positions with Milky Way background')
    assert isinstance(plotsname, str)
    name = '_mw_' + coord + '_' + mode
    if z is not None:
        name += '_colorcoded_' + z[1].split(' ')[0]
    name = tool.remove_dumbchars(name)
    name += '.png'
    name = plotsname + name

    x = x.data
    y = y.data

    plot_instance = MWPlot(radius=radius*u.kpc, unit=u.kpc,
                           coord=coord, mode=mode, grayscale=True,
                           annotation=False)
    # Set alpha value for MW image
    plot_instance.imalpha = 0.80
    plot_instance.cmap = 'plasma'
    plot_instance.tight_layout = True
    if z is not None:
        plot_instance.clim = clim
        plot_instance.mw_scatter(x * u.kpc, y * u.kpc, z)
    else:
        plot_instance.mw_scatter(x * u.kpc, y * u.kpc, c='r')
    plot_instance.savefig(name)
    plt.close()
    print('Done!')


def mwskymap(plotsname, ra, dec, z=None, clim=None):
    """
    Use MWSkyPlot to make a celestial map with the Galaxy in the background

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    ra : astropy Column
        Right ascension of the sample in degrees.
    dec : astropy Column
        Declination of the sample in degrees.
    z : None or list with astropy Column and str, optional
        Column to colorcode by along with colorbar title [z, 'colorbar_title'].
        If None no colorcoding is used.  Default is None
    clim : tuble or None, optional
        Colorbar range
    """
    print('Make celestial plot of galactic positions with Milky Way background')
    assert isinstance(plotsname, str)
    name = plotsname + '_mwskymap'
    if z is not None:
        name += '_colorcoded_' + z[1].split(' ')[0]
    name += '.png'

    floatfill = funkycat.get_fillvalue(ra)
    fillmask = (ra != floatfill) & (dec != floatfill)
    ra = ra[fillmask].data * u.degree
    dec = dec[fillmask].data * u.degree

    plot_mw = MWSkyMap(projection='mollweide')

    # Set alpha value for MW image
    plot_mw.imalpha = 0.80
    plot_mw.cmap = 'viridis'

    c = SkyCoord(ra=ra, dec=dec)
    c = c.galactic
    if z is not None:
        plot_mw.mw_scatter(c.l, c.b, z)
        plot_mw.clim = clim
    else:
        plot_mw.mw_scatter(c.l, c.b, c='r')
    plot_mw.savefig(name)
    plt.close()
    print('Done!')


def tutorialplot(plotsname, lz, jR, theta_R, theta_phi):
    """
    Replicate plots from Jason Sanders's galpy tutorial.

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    """
    print('Make plot from tutorial')
    assert isinstance(plotsname, str)
    name = plotsname + '_tutorial'
    name += '.pdf'
    pp = PdfPages(name)

    gs=50
    ccmap='inferno'
    f, ((ax1, ax2),(ax3, ax4)) = plt.subplots(2, 2, sharey='row', sharex='col', figsize=(18, 18))
    theta_p = np.copy(theta_phi)
    ax1.hexbin(lz, np.sqrt(jR * 220. * 8.34),# extent=[0.5, 1.29, 0, 16],
               bins='log', gridsize=gs, cmap=ccmap, rasterized=True, mincnt=1)
    # ax1.set_xlim([0.5,1.29])
    # ax1.set_ylim([0,16])
    ax1.set_ylabel(r'$\sqrt{J_R}\ (8.34\ \mathrm{kpc}\times220\ \mathrm{km\ s}^{-1})$', fontsize=24)
    ax2.hexbin(theta_R, np.sqrt(jR*220.*8.34), #extent=[0,2.*np.pi,0,16],
               bins='log', gridsize=gs, cmap=ccmap, rasterized=True, mincnt=1)
    # ax2.set_xlim([0.,2.*np.pi])
    # ax2.set_ylim([0,16])
    ax3.hexbin(lz, theta_p,  #, extent=[0.5,1.29,-1,0.99])
               bins='log', gridsize=gs, cmap=ccmap, rasterized=True, mincnt=1)
    # ax3.set_xlim([0.5,1.29])
    # ax3.set_ylim([-1,1])
    ax3.set_xlabel(r'$L_z\ (8\ \mathrm{kpc}\times220\ \mathrm{km\ s}^{-1})$', fontsize=24)
    ax3.set_ylabel(r'$\theta_{\phi}\ (\mathrm{rad})$', fontsize=24)
    ax4.hexbin(theta_R, theta_p, #extent=[0,2.*np.pi,-1,0.99],
               gridsize=gs, bins='log', cmap=ccmap, rasterized=True, mincnt=1)
    # ax4.set_xlim([0.,2.*np.pi])
    # ax4.set_ylim([-1,1])
    ax4.set_xlabel(r'$\theta_R\ (\mathrm{rad})$', fontsize=24)
    plt.subplots_adjust(wspace=0.)
    plt.subplots_adjust(hspace=0.)
    ax1.tick_params(axis='both', which='major', labelsize=20)
    ax2.tick_params(axis='both', which='major', labelsize=20)
    ax3.tick_params(axis='both', which='major', labelsize=20)
    ax4.tick_params(axis='both', which='major', labelsize=20)

    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


###############################################################################
# Plots for checking flags and quality
###############################################################################
def flagplot(plotsname, cat, starid, flags, success, colors):
    """
    Make an overview plot for different flags.
    This works best for a few selected stars.

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    cat : astropy Table
        Table containing all flags in `flags`.
    starid : str
        Name of column in `cat` containing the star ids.
    flags : list
        list of column names in cat corresponding to the flags you want to
        get an overview of.
    success : list
        list of success criteria for the different flags in the same order as
        `flags`.
    colors : dict
        Colors of the stars in a dict with starid as a key.
    """
    assert len(flags) == len(success)
    print('Make plot from tutorial')
    assert isinstance(plotsname, str)
    name = plotsname + '_flags'
    name += '.pdf'
    pp = PdfPages(name)

    stars = cat[starid]
    fig, axs = plt.subplots(1, 1)
    axs.set_xticks(np.arange(len(stars)))
    axs.set_yticks(np.arange(len(flags)))
    axs.invert_xaxis()
    axs.invert_yaxis()
    axs.set_xticklabels(stars, rotation=50)
    axs.set_yticklabels(flags)
    for i, star in enumerate(stars):
        for j, flag in enumerate(flags):
            flagfill = funkycat.get_fillvalue(cat[flag])
            if cat[flag][i] == flagfill:
                # Make a cross
                axs.scatter(i, j, marker='x', c=colors[star])
            elif cat[flag][i] == success[j]:
                # Make a white circle
                axs.scatter(i, j, marker='o', edgecolors=colors[star],
                            c='1.0')
            else:
                # Make coloured circle
                axs.scatter(i, j, marker='o', edgecolors=colors[star],
                            c=colors[star])

    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def compare_coords(plotsname, cat, ra, dec, idstr,
                   gaia=True, trackstars=False):
    """
    Compare coordinates for different surveys by default to Gaia.

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    cat : astropy Table
        Table containing all flags in `flags`.
    ra : str
        Name of column in `cat` containing the right ascension.
    dec : str
        Name of column in `cat` containing the declination.
    idstr : str
        ID of the survey of `ra` and `dec`.
    gaia : bool, optional
        Should this compare to Gaia coordinates (True) or 2MASS (False).
    trackstars : bool, optional
        Should this return a list of TMASS ids for the stars with a
        separation greater than 2 arcseconds?
    """
    print('Compare coordinates')
    if gaia:
        cra = 'RA_GAIA'
        cdec = 'DEC_GAIA'
        cidstr = 'Gaia'
    else:
        cra = 'RA_J2000_TMASS'
        cdec = 'DEC_J2000_TMASS'
        cidstr = '2MASS'

    print('Compare to %s' % cidstr)
    floatfill = funkycat.get_fillvalue(cat[ra])
    mask = ((cat[cra] != floatfill) & (cat[ra].data != 0.0) &
            (cat[ra] != floatfill))
    c1 = coord.SkyCoord(cat[cra][mask],
                  cat[cdec][mask])
    c2 = coord.SkyCoord(cat[ra].data[mask] * u.deg,
                  cat[dec].data[mask] * u.deg)
    sep = c1.separation(c2).arcsecond
    scatter(plotsname,
            ['RA (%s)' % cidstr,
             'Angular separation between %s and %s [as]' % (cidstr, idstr)],
            [cat[cra][mask], sep])

    mask &= ((cat[ra].data != floatfill) & (cat[ra].data != 0.0) &
             (cat[cra].data != floatfill))

    c1 = coord.SkyCoord(cat[cra][mask],
                  cat[cdec][mask])
    c2 = coord.SkyCoord(cat[ra].data[mask] * u.deg,
                  cat[dec].data[mask] * u.deg)
    sep = c1.separation(c2).arcsecond

    scatter(plotsname,
            ['RA (%s)' % cidstr,
             'Angular separation between %s and %s [as]' % (cidstr, idstr)],
            [cat[cra][mask], sep])

    seps = cat['SOURCE_ID_GAIA'][mask][sep > 2]
    if len(seps) > 0:
        print('The Gaia IDs for targets with an angular separation greater than 2 as are')
        print('| SOURCE_ID_GAIA | TMASSID | Separation (as) |')
        print('| --- | --- | ---- |' )
        for i in range(len(seps)):
            print('| %s | %s | %s |' % (
                seps[i], cat['TMASS_ID'][mask][sep > 2][i], sep[sep > 2][i]))

    scatter(plotsname,
            ['RA (%s)' % cidstr,
             'Angular separation between %s and %s [as]' % (cidstr, idstr),
             'Proper motion (Gaia)'],
            [cat[cra][mask], sep,
             np.sqrt(cat['PMRA_GAIA'][mask] ** 2 + cat['PMDEC_GAIA'][mask] ** 2)])

    print('Done!')
    if trackstars:
        # Track stars
        badstars = np.isin(cat['TMASS_ID'], cat['TMASS_ID'][mask][sep > 2])
        return badstars


def iddifference(plotsname, cat, id1, id2, idstrs, test1, test2,
                 idstring=False):
    """
    Find stars with different ids (where the crossmatch might are wrong).

    Parameters
    ----------
    plotsname : str
        samplename used for file names of the saved plots.
    cat : astropy Table
        Table containing all flags in `flags`.
    id1 : str
        Name of column in `cat` containing one ID
    id2 : str
        Name of column in `cat` containing the other ID
    idstrs : list
        List of len(idstrs) == 2 with a description of `id1` and `id2`.
    test1 : str
        Name of column in `cat` containing a test parameter for the crossmatch
        for `id1`.
    test2 : str
        Name of column in `cat` containing a test parameter for the crossmatch
        for `id2`.
    """
    print('Plot difference between %s and %s')
    id1fillvalue = funkycat.get_fillvalue(cat[id1])
    id2fillvalue = funkycat.get_fillvalue(cat[id2])
    test1fillvalue = funkycat.get_fillvalue(cat[test1])
    test2fillvalue = funkycat.get_fillvalue(cat[test2])

    notempty = ((cat[id1] != id1fillvalue) &
                (cat[id2] != id2fillvalue) & (cat[id2] != 0) &
                (cat[test1] != test1fillvalue) &
                (cat[test2] != test2fillvalue))
    print('Overlap between %s and %s is' % (
        idstrs[0], idstrs[1]), np.sum(notempty))

    notequal = (cat[id1].astype(str) != cat[id2].astype(str))
    notequal &= notempty
    if np.sum(notequal) > 0:
        print('The Gaia IDs for targets with different IDS are')
        wrongid1 = cat[id1][notequal]
        wrongid2 = cat[id2][notequal]
        wrongtmassid = cat['TMASS_ID'][notequal]
        print('| %s | %s | TMASSID |' % (id1, id2))
        print('| --- | --- | ---- |')
        for i, (wid1, wid2, wtmassid) in enumerate(zip(wrongid1, wrongid2, wrongtmassid)):
            print('| %s | %s | %s |' % (wid1, wid2, wtmassid))

    if not idstring:
        scatter(plotsname,
                ['%s' % test1,
                 '%s - %s (%s - %s)' % (test1, test2, idstrs[0], idstrs[1])],
                [cat[test1][notequal].astype(int),
                 (cat[test1][notequal] -
                  cat[test2][notequal])])

    mask = notequal

    if np.sum(mask) > 0:
        print('The Gaia IDs for targets with different IDS in subsample are')
        wrongid1 = cat[id1][mask]
        wrongid2 = cat[id2][mask]
        wrongtmassid = cat['TMASS_ID'][mask]
        print('| %s | %s | TMASSID |' % (id1, id2))
        print('| --- | --- | ---- |')
        for i, (wid1, wid2, wtmassid) in enumerate(zip(wrongid1, wrongid2, wrongtmassid)):
            print('| %s | %s | %s |' % (wid1, wid2, wtmassid))

    scatter(plotsname,
            ['%s' % test1,
             '%s - %s (%s - %s)' % (test1, test2, idstrs[0], idstrs[1])],
            [cat[test1][notequal].astype(int),
             (cat[test1][notequal] -
              cat[test2][notequal])])


###############################################################################
# BASTA plots
###############################################################################
def compare_basta(plotsname, cat1, cat2, id1, id2,
                  BASTA_cols, BASTA_uerr=None, BASTA_lerr=None,
                  masks=None, masksidstrs=None, norange=False, cc=None, cid=None):

    for i, pbasta in enumerate(BASTA_cols):
        if BASTA_uerr is None:
            xlower = cat1['LOWER_ERROR_' + pbasta].data
            xupper = cat1['UPPER_ERROR_' + pbasta].data
            ylower = cat2['LOWER_ERROR_' + pbasta].data
            yupper = cat2['UPPER_ERROR_' + pbasta].data
        else:
            xlower = cat1[BASTA_lerr[i]].data
            xupper = cat1[BASTA_uerr[i]].data
            ylower = cat2[BASTA_lerr[i]].data
            yupper = cat2[BASTA_uerr[i]].data

        if cc is not None:
            scatter(plotsname,
                    [pbasta + '_' + id1, pbasta + '_' + id2, cid],
                    [cat1[pbasta], cat2[pbasta], cc], compare=True,
                    masks=masks, masksidstrs=masksidstrs)
        else:
            scatter(plotsname,
                    [pbasta + '_' + id1, pbasta + '_' + id2],
                    [cat1[pbasta], cat2[pbasta]], compare=True,
                    xerr=[xlower, xupper], yerr=[ylower, yupper],
                    masks=masks, masksidstrs=masksidstrs)


def plot_flagbasta(plotsname, tables, tableidstrs):
    """
    Color code by flags in tables[0]
    """
    flags = [col for col in tables[0].columns
             if col.startswith('FLAG_') and col.endswith('_BASTA')]
    params = [flag[5:] for flag in flags]
    for flag, param in zip(flags, params):
        if param == 'MEH_BASTA':
            param = 'ME_H_BASTA'
        if flag in tables[0].columns and flag in tables[1].columns:
            flagmask = tables[0][flag] > 0
            compare_basta(plotsname,
                          tables[0][flagmask], tables[1][flagmask],
                          tableidstrs[0], tableidstrs[1],
                          [param], ['UPPER_ERROR_' + param],
                          ['LOWER_ERROR_' + param],
                          cc=tables[0][flag][flagmask], cid='Standard deviations')
###############################################################################
# Deprecated old plots
###############################################################################
def compare_plxdist(plotsname, plx, dist):
    print('Plot: Compare plx to BASTA dist')
    assert isinstance(plotsname, str)
    dist = dist.astype(float)
    name = plotsname + '_comparison_plxdist.pdf'

    plxfilter = (plx < 0)
    plx = plx[~plxfilter]
    dist = dist[~plxfilter]
    plx = plx * 1e-3  # u.mas, now in as
    plx_dist = 1 / plx
    diff = (plx_dist - dist) * (1e-3)

    pp = PdfPages(name)
    plt.figure()
    y, x, _ = plt.hist(diff, density=False, bins='fd')
    plt.axvline(x=0, linestyle='--', c='0.6')
    elem = np.argmax(y)
    plt.axvline(x=x[elem], linestyle=':', c='0.6')
    plt.xlabel(r'$\varpi^{-1} - d_{\mathrm{BASTA}}$ (kpc)')
    plt.xlim([-2, 2])
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def plot_compare_ages(plotsname, bastaages, compareages):
    print('Plot: Compare ages to BASTA ages')
    assert isinstance(plotsname, str)
    name = plotsname + '_comparison_ages.pdf'
    bastaages *= 1e-3
    compareages *= 1e-3

    diff = (compareages - bastaages)

    pp = PdfPages(name)
    plt.figure()
    y, x, _ = plt.hist(diff, density=False, bins='fd')
    plt.axvline(x=0, linestyle='--', c='0.6')
    elem = np.argmax(y)
    plt.axvline(x=x[elem], linestyle=':', c='0.6')
    plt.xlabel(r'Age$_{\mathrm{Sanders}}$ - Age$_{\mathrm{BASTA}}$ (Gyr)')
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def plot_3differences(plotsname, kitten1, kitten2, labels, whatminuswhat):
    print('Make histogram of differences between kittens')
    assert isinstance(plotsname, str)
    assert isinstance(whatminuswhat, str)
    assert len(labels) == 3
    name = plotsname + '_diff_' + whatminuswhat + '.pdf'
    pp = PdfPages(name)
    f, axs = plt.subplots(1, 3, sharey=True)
    for (label, ax) in zip(labels, axs):
        x1 = kitten1[label].data.astype(float)
        x2 = kitten2[label].data.astype(float)
        real1 = (x1 != fillvalue)
        real2 = (x2 != fillvalue)
        x1 = x1[real1]
        x2 = x2[real2]
        overlap = np.where(np.in1d(kitten1['TMASS_ID'].data[real1],
                                   kitten2['TMASS_ID'].data[real2]))[0]
        diff = x1[overlap] - x2
        sns.distplot(diff, hist=True, kde=True, bins=None, ax=ax)
        ax.set_xlabel(label)
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def plot_uncertainty_kde(plotsname, err_u, err_v, err_w,
                         redgauss=False, oldlimits=False):
    print('Make kernel density plot of uncertainties')
    assert isinstance(plotsname, str)
    name = plotsname + '_uncertainties_kde.pdf'

    kdeu = sm.nonparametric.KDEUnivariate(err_u)
    kdev = sm.nonparametric.KDEUnivariate(err_v)
    kdew = sm.nonparametric.KDEUnivariate(err_w)
    kdeu.fit(kernel='gau', bw='scott')
    kdev.fit(kernel='gau', bw='scott')
    kdew.fit(kernel='gau', bw='scott')

    def gauss(x, a, x0, sigma):
        return a * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))

    def fitgauss(x, y):
        mean = np.sum(x * y) / (np.sum(x))
        sigma = np.sqrt(np.sum(x - y) / len(x))
        popt, pcov = curve_fit(gauss, x, y, p0=[1, mean, sigma])
        print('Gauss fitting parameters are')
        print(popt)
        return popt

    poptu = fitgauss(kdeu.support, kdeu.density)
    poptv = fitgauss(kdev.support, kdev.density)
    poptw = fitgauss(kdew.support, kdew.density)

    pp = PdfPages(name)
    f, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True)
    ax1.set_ylabel('Kernel density')
    ax1.set_xlabel(r'$\sigma_U$ (km/s)')
    ax2.set_xlabel(r'$\sigma_V$ (km/s)')
    ax3.set_xlabel(r'$\sigma_W$ (km/s)')
    ax1.plot(kdeu.support, kdeu.density)
    ax2.plot(kdev.support, kdev.density)
    ax3.plot(kdew.support, kdew.density)
    if redgauss:
        ax1.plot(kdeu.support, gauss(kdeu.support, *poptu), 'r')
        ax2.plot(kdev.support, gauss(kdev.support, *poptv), 'r')
        ax3.plot(kdew.support, gauss(kdew.support, *poptw), 'r')
    if oldlimits:
        ax1.set_xlim([0, 35])
        ax2.set_xlim([0, 12])
        ax3.set_xlim([0, 27.5])
    else:
        ax1.set_xlim([0, 10])
        ax2.set_xlim([0, 10])
        ax3.set_xlim([0, 10])
    # ax1.set_ylim([0, 1.29])
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def plot_uncertainty_hist(plotsname, err_u, err_v, err_w):
    print('Plot histogram of uncertainties')
    assert isinstance(plotsname, str)
    name = plotsname + '_uncertainties_hist.pdf'
    pp = PdfPages(name)
    plt.figure()
    plt.hist(err_u, density=False, alpha=0.5, bins='fd', log=False,
             zorder=0, label=r'$\sigma_U$')
    plt.hist(err_v, density=False, alpha=0.5, bins='fd', log=False,
             zorder=1, label=r'$\sigma_V$')
    plt.hist(err_w, density=False, alpha=0.5, bins='fd', log=False,
             zorder=2, label=r'$\sigma_W$')
    plt.xlim(0, 25)
    plt.legend(frameon=False)
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def plot_uncertainty_distance(plotsname, dist, err_u, err_v, err_w,
                              W=303):
    print('Plot uncertainty as a function of distance')
    assert W % 2 == 1
    assert isinstance(plotsname, str)
    name = plotsname + '_uncertainties_dist_W' + str(W) + '.pdf'
    dist = dist * 1e-3  # in kpc
    pp = PdfPages(name)
    plt.figure()

    # Sort everything according to distance
    sortfilter = np.argsort(dist)
    distsort = dist[sortfilter]
    err_usort = err_u[sortfilter]
    err_vsort = err_v[sortfilter]
    err_wsort = err_w[sortfilter]
    plt.plot(dist[sortfilter], err_u[sortfilter], '.',
             rasterized=True, alpha=0.4)
    plt.plot(dist[sortfilter], err_v[sortfilter], '.',
             rasterized=True, alpha=0.4)
    plt.plot(dist[sortfilter], err_w[sortfilter], '.',
             rasterized=True, alpha=0.4)

    # Define the pass width
    pw = int((W - 1) / 2)
    c = ['b', 'r', 'g']
    # use 10% or 15% darker colors for lines
    # (hexcode from https://www.hexcolortool.com)
    dc10 = ["#005999", "#00855A", "#BC4500", "#B3608E", "#D7CB29", "#3D9BD0"]
    dc15 = ["#004CBC", "#AF3800", "#00784D"]
    labels = [r'$U$', r'$V$', r'$W$']

    # Calculate running quantiles
    for i, err in enumerate([err_usort, err_vsort, err_wsort]):
        med = np.percentile(tool.strided_app(err, W, 1), 50, axis=-1)
        med = np.pad(med, pw, 'constant',
                     constant_values=np.nan)
        lq = np.percentile(tool.strided_app(err, W, 1), 25, axis=-1)
        lq = np.pad(lq, pw, 'constant',
                    constant_values=np.nan)
        hq = np.percentile(tool.strided_app(err, W, 1), 75, axis=-1)
        hq = np.pad(hq, pw, 'constant',
                    constant_values=np.nan)
        plt.plot(distsort, med, '-', c=dc15[i], label=labels[i])
    plt.xlabel('Distance (kpc)')
    plt.ylabel(r'$\sigma$ (km/s)')
    plt.ylim([0.05, 17.5])
    plt.xlim([distsort[pw], distsort[-pw]])
    plt.legend(frameon=False)
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')

    print('Plot uncertainty as a function of distance quantiles')
    name = plotsname + '_quantiles_uncertainties_dist_W' + str(W) + '.pdf'
    pp = PdfPages(name)
    plt.figure()
    for i, err in enumerate([err_usort, err_vsort, err_wsort]):
        med = np.percentile(tool.strided_app(err, W, 1), 50, axis=-1)
        med = np.pad(med, pw, 'constant',
                     constant_values=np.nan)
        lq = np.percentile(tool.strided_app(err, W, 1), 25, axis=-1)
        lq = np.pad(lq, pw, 'constant',
                    constant_values=np.nan)
        hq = np.percentile(tool.strided_app(err, W, 1), 75, axis=-1)
        hq = np.pad(hq, pw, 'constant',
                    constant_values=np.nan)
        plt.fill_between(distsort, hq, lq, color=c[i], alpha=0.4)
        plt.plot(distsort, med, '-', c=c[i], label=labels[i])
    plt.xlabel('Distance (kpc)')
    plt.ylabel(r'$\sigma$ (km/s)')
    plt.ylim([0.05, 17.5])
    plt.xlim([distsort[pw], distsort[-pw]])
    plt.legend(frameon=False)
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')


def plot_uncertainty_velocity(plotsname, u, v, w, err_u, err_v, err_w, W=303):
    print('Plot the uncertainties as a function of velocity')
    assert W % 2 == 1
    assert isinstance(plotsname, str)
    name = plotsname + '_uncertainties_velocity_W' + str(W) + '.pdf'

    # Define the pass width
    pw = int((W - 1) / 2)
    c = ['b', 'r', 'g']
    # use 10% or 15% darker colors for lines
    # (hexcode from https://www.hexcolortool.com)
    dc10 = ["#005999", "#00855A", "#BC4500", "#B3608E", "#D7CB29", "#3D9BD0"]
    dc15 = ["#004CBC", "#AF3800", "#00784D"]
    labels = [r'$U$', r'$V$', r'$W$']

    pp = PdfPages(name)
    plt.figure()

    # Calculate running quantiles
    vels = [u, v, w]
    errs = [err_u, err_v, err_w]
    for i, (vel, err) in enumerate(zip(vels, errs)):
        sortfilter = np.argsort(vel)
        velsort = vel[sortfilter]
        errsort = err[sortfilter]
        plt.plot(velsort, errsort, '.', markersize=1, alpha=0.4, zorder=i)
        med = np.percentile(tool.strided_app(errsort, W, 1), 50, axis=-1)
        med = np.pad(med, pw, 'constant', constant_values=np.nan)
        lq = np.percentile(tool.strided_app(errsort, W, 1), 25, axis=-1)
        lq = np.pad(lq, pw, 'constant', constant_values=np.nan)
        hq = np.percentile(tool.strided_app(errsort, W, 1), 75, axis=-1)
        hq = np.pad(hq, pw, 'constant', constant_values=np.nan)
        plt.plot(velsort, med, '-', c=dc15[i], label=labels[i],
                 zorder=4)
    plt.xlabel('Velocity (km/s)')
    plt.ylabel(r'$\sigma$ (km/s)')
    plt.yscale('log')
    plt.xlim([-500, 500])
    plt.legend(frameon=False)
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')

    print('Plot uncertainties in velocity using quantiles')
    name = plotsname + '_quantiles_uncertainties_velocity_W' + str(W) + '.pdf'
    pp = PdfPages(name)
    plt.figure()

    # Calculate running quantiles
    for i, (vel, err) in enumerate(zip(vels, errs)):
        sortfilter = np.argsort(vel)
        velsort = vel[sortfilter]
        errsort = err[sortfilter]
        med = np.percentile(tool.strided_app(errsort, W, 1), 50, axis=-1)
        med = np.pad(med, pw, 'constant', constant_values=np.nan)
        lq = np.percentile(tool.strided_app(errsort, W, 1), 25, axis=-1)
        lq = np.pad(lq, pw, 'constant', constant_values=np.nan)
        hq = np.percentile(tool.strided_app(errsort, W, 1), 75, axis=-1)
        hq = np.pad(hq, pw, 'constant', constant_values=np.nan)
        plt.fill_between(velsort, hq, lq, color=c[i], alpha=0.4)
        plt.plot(velsort, med, '-', c=c[i], label=labels[i])
    plt.xlabel('Velocity (km/s)')
    plt.ylabel(r'$\sigma$ (km/s)')
    plt.yscale('log')
    plt.xlim([-190, 190])
    plt.legend(frameon=False)
    pp.savefig(bbox_inches='tight')
    pp.close()
    print('Done!')
